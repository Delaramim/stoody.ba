import React, { createContext, useContext, useState } from "react";

export const Context = createContext({});

export const Provider = (props) => {
  const { children } = props;

  const [profileModal, setProfileModal] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [jobTitle, setJobTitle] = useState("");
  const [mainServices, setManiServices] = useState("");
  const [skills, setSkills] = useState([]);
  const [languages, setLanguages] = useState([]);
  const [educations, setEducations] = useState([]);
  const [isStudent, setIsStudent] = useState(true);
  const [bio, setBio] = useState("");
  const [experiences, setExperiences] = useState([]);
  const [edit, setEdit] = useState(false);

  // Make the context object:
  const UserContext = {
    edit,
    setEdit,
    profileModal,
    setProfileModal,
    lastName,
    setLastName,

    username,
    setUsername,
    password,
    setPassword,
    jobTitle,
    setJobTitle,
    mainServices,
    setManiServices,
    skills,
    setSkills,
    educations,
    setEducations,
    languages,
    setLanguages,
    firstName,
    setFirstName,

    isStudent,
    setIsStudent,
    bio,
    setBio,
    experiences,
    setExperiences,
  };

  // pass the value in provider and return
  return <Context.Provider value={UserContext}>{children}</Context.Provider>;
};

export const { Consumer } = Context;

export const useUserContext = () => useContext(Context);
