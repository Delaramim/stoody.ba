import React from "react";
import { useUserContext, Provider as UserProvider } from "./userContext";
import { useAuthContext, Provider as AuthProvider } from "./authContext";
import { useTaskContext, Provider as TaskProvider } from "./taskContext";

const ContextProvider = ({ children }) => {
  return (
    <AuthProvider>
      <TaskProvider>
        <UserProvider>{children}</UserProvider>
      </TaskProvider>
    </AuthProvider>
  );
};

export { useUserContext, useAuthContext, ContextProvider, useTaskContext };
