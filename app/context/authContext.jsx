import React, { useState, useEffect, useContext, createContext } from "react";
import "firebase/firestore";

import { auth } from "firebase/app";
// import { fetchUser } from "../api/UserApi";
require("firebase/auth");

export const Context = createContext();

export const Provider = (props) => {
  const [redirectToReferrer, setRedirectToReferrer] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [user, setUser] = useState();
  const [newUser, setNewUser] = useState(false);
  const [student, setStudent] = useState(null);
  const [expoPushToken, setExpoPushToken] = useState("");
  const [passwordSuccess, setPasswordSuccess] = useState(false);

  // console.log(user);

  // const getStudent = (student) => {
  //   if (user.uid) setStudent(student);
  // };

  // if (user && !student) {
  //   console.log("EWGEGEH");
  //   fetchUser(user.uid, getStudent);
  // }

  // console.log("USER", user);

  const clearState = () => {
    setRedirectToReferrer(false);
    setStudent(null);
    setUser(null);
    setError(null);
  };

  const { children } = props;

  const doSignUp = ({ email, password }) => {
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        console.log("User account created & signed in!");
      })
      .catch((error) => {
        if (error.code === "auth/email-already-in-use") {
          console.log("That email address is already in use!");
        }

        if (error.code === "auth/invalid-email") {
          console.log("That email address is invalid!");
        }

        setError(error.message);
      });
  };
  const doSignIn = ({ email, password }) => {
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        setStudent("Someone");
        console.log("Welcome");
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  const doPasswordReset = async (data) => {
    console.log(data.email);
    auth()
      .sendPasswordResetEmail(data.email)
      .then(function () {
        setPasswordSuccess(true);
      })
      .catch(function (error) {
        setError(error.message);
      });
  };
  const doSignOut = async () => {
    auth().signOut();
    clearState();
    console.log("User signed out");
  };

  // Make the context object:
  const authContext = {
    expoPushToken,
    setExpoPushToken,
    student,
    user,
    setUser,
    doSignUp,
    doSignOut,
    doPasswordReset,
    newUser,
    setNewUser,
    redirectToReferrer,
    loading,
    error,
    doPasswordReset,
    doSignIn,
    setStudent,
    passwordSuccess,
    setError,
  };

  // pass the value in provider and return
  return <Context.Provider value={authContext}>{children}</Context.Provider>;
};

export const { Consumer } = Context;

export const useAuthContext = () => useContext(Context);
