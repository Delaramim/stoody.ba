import React, { createContext, useContext, useState } from "react";

export const Context = createContext({});

export const Provider = (props) => {
  const { children } = props;
  const [taskName, setTaskName] = useState(null);
  const [taskDescription, setTaskDescription] = useState(null);
  const [requirements, setRequirements] = useState([]);
  const [tasks, setTasks] = useState([]);
  const [isSearching, setIsSearching] = useState(false);
  const [searchedTasks, setSearchedTasks] = useState([]);

  // Make the context object:
  const taskContext = {
    taskName,
    setTaskName,
    taskDescription,
    setTaskDescription,
    requirements,
    setRequirements,
    tasks,
    setTasks,
    searchedTasks,
    setSearchedTasks,
    isSearching,
    setIsSearching,
  };

  // pass the value in provider and return
  return <Context.Provider value={taskContext}>{children}</Context.Provider>;
};

export const { Consumer } = Context;

export const useTaskContext = () => useContext(Context);
