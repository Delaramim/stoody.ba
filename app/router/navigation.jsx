import React, { useState, useEffect } from "react";
import { auth } from "firebase/app";
require("firebase/auth");

import {
  Login,
  TaskScreen,
  WelcomeScreen,
  SignUpScreen,
  CreateTask,
  OthersProfile,
  BioEdit,
  ExperienceEdit,
  EducationEdit,
  SkillsEdit,
  LanguageEdit,
  TaskEdit,
  PasswordReset,
} from "../screens/index";

import { Image, Button, Text } from "react-native";

import {
  HomeNavigation,
  RegistrationNavigation,
  EditNavigation,
} from "./navigations/index";
import { useAuthContext, useUserContext } from "../context";
import { NavigationContainer, useNavigation } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HeaderRight from "../components/HeaderRight";

const Stack = createStackNavigator();

const LogoTitle = () => {
  return (
    <Image
      style={{ width: 81, height: 14 }}
      source={require("../assets/stoodyMini.png")}
    />
  );
};

export default function App() {
  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const { user, setUser, completed, student, setStudent } = useAuthContext();

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;

  return (
    <NavigationContainer>
      {!user || !student ? (
        <Stack.Navigator>
          <Stack.Screen
            options={{ headerShown: false }}
            name="Welcome"
            component={WelcomeScreen}
          />
          <Stack.Screen
            name="Login"
            component={Login}
            options={{
              headerStyle: {
                backgroundColor: "#9DA6EB",
                borderBottomColor: "#9DA6EB",
                shadowColor: "null",
              },
              headerTintColor: "white",
              headerTitleStyle: {
                fontWeight: "bold",
              },
            }}
          />
          <Stack.Screen
            name="Reset"
            component={PasswordReset}
            options={{
              headerStyle: {
                backgroundColor: "#9DA6EB",
                borderBottomColor: "#9DA6EB",
                shadowColor: "null",
              },
              headerTintColor: "white",
              headerTitleStyle: {
                fontWeight: "bold",
              },
            }}
          />
          <Stack.Screen name="Sign up" component={SignUpScreen} />

          {user ? (
            <Stack.Screen
              name="Register"
              options={{ headerShown: false }}
              component={RegistrationNavigation}
            />
          ) : null}
        </Stack.Navigator>
      ) : (
        <Stack.Navigator>
          <Stack.Screen
            name={"Home"}
            options={({ route }) => ({
              headerTitle: () => <LogoTitle />,
              headerRight: () => <HeaderRight route={route}></HeaderRight>,

              headerLeft: null,
            })}
            component={HomeNavigation}
          />

          <Stack.Screen
            name="CreateTask"
            component={CreateTask}
            options={{ title: "New task" }}
          />
          <Stack.Screen name="Task" component={TaskScreen} />
          <Stack.Screen
            name="OthersProfile"
            component={OthersProfile}
            options={{ headerTitle: "Profile" }}
          />
          <Stack.Screen
            name="Bio"
            component={BioEdit}
            options={{ headerBackTitle: "" }}
          />
          <Stack.Screen
            name="Experience"
            component={ExperienceEdit}
            options={{ headerBackTitle: "" }}
          />
          <Stack.Screen
            name="Education"
            component={EducationEdit}
            options={{ headerBackTitle: "" }}
          />
          <Stack.Screen
            name="Skills"
            component={SkillsEdit}
            options={{ headerBackTitle: "" }}
          />
          <Stack.Screen
            name="Languages"
            component={LanguageEdit}
            options={{ headerBackTitle: "" }}
          />
          <Stack.Screen
            name="TaskEdit"
            component={TaskEdit}
            options={{ headerBackTitle: "", headerTitle: "Edit" }}
          />
        </Stack.Navigator>
      )}
    </NavigationContainer>
  );
}
