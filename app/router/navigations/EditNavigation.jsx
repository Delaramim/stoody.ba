import React, { useState } from "react";

import { BioEdit } from "../../screens/index";

import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const Edit = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Bio"
        component={BioEdit}
        options={{
          headerLeft: null,
        }}
      />
    </Stack.Navigator>
  );
};

export default Edit;
