import React, { useState } from "react";

import {
  EducationRegister,
  ExperienceRegister,
  FeeRegister,
  JobRegister,
  LanguageRegister,
  RegisterScreen,
  RegistrationComplete,
  SkillsRegister,
} from "../../screens/index";

import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const Register = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Jobs"
        component={JobRegister}
        options={{
          headerLeft: null,
        }}
      />
      <Stack.Screen name="Languages" component={LanguageRegister} />
      <Stack.Screen name="Education" component={EducationRegister} />
      <Stack.Screen name="Fee" component={FeeRegister} />
      <Stack.Screen name="Experience" component={ExperienceRegister} />
      <Stack.Screen name="Skills" component={SkillsRegister} />
      <Stack.Screen
        name="Complete"
        component={RegistrationComplete}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default Register;
