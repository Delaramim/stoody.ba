import HomeNavigation from "./HomeNavigation";
import RegistrationNavigation from "./RegistrationNavigation";
import EditNavigation from "./EditNavigation";

export { HomeNavigation, RegistrationNavigation, EditNavigation };
