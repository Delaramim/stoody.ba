import React, { useState } from "react";
import Steps from "../../components/Steps";
import { useForm, Controller } from "react-hook-form";
import { View, StyleSheet, Text } from "react-native";
import { Button, Item, Icon, Form, Label } from "native-base";
import Footer from "../../components/FooterRegister";
import FormInput from "../../components/FormIconInput";
import Skill from "../../components/Requirement";
import { ScrollView } from "react-native-gesture-handler";
import { useUserContext } from "../../context";

const Skills = ({ navigation }) => {
  const { skills, setSkills } = useUserContext();
  const { control, handleSubmit, errors } = useForm();

  const onSubmit = (data) => {
    if (data.skill != "") {
      setSkills((oldData) => [...oldData, data]);
    }
  };

  const handleDelete = (skill) => {
    setSkills(skills.filter((i) => i != skill));
  };
  return (
    <View style={styles.container}>
      <Steps percentage={"40%"}></Steps>
      <View style={styles.box}>
        <Form>
          <Text style={styles.title}>Input your skills </Text>
          <View style={styles.row}>
            <View style={styles.input}>
              <FormInput
                placeholder="Add skill"
                name={"skill"}
                control={control}
                text={"Title"}
                clearButtonMode="always"
              />
            </View>
            <Button style={styles.button} onPress={handleSubmit(onSubmit)}>
              <Text style={styles.buttonText}>Add</Text>
            </Button>
          </View>
          <ScrollView>
            {skills?.map((skill, index) => {
              return (
                <View style={styles.row2} key={index}>
                  <Skill text={skill.skill} />

                  <Icon
                    name={"close-outline"}
                    type="Ionicons"
                    style={styles.trash}
                    onPress={() => handleDelete(skill)}
                  />
                </View>
              );
            })}
          </ScrollView>
        </Form>
      </View>
      <Footer
        nextPage={"Experience"}
        text={"Next"}
        onPress={() => navigation.navigate("Education")}
        backgroundColor={"#34C787"}
        color={"white"}
      ></Footer>
    </View>
  );
};
const styles = StyleSheet.create({
  trash: {
    fontSize: 20,
    color: "#C3BFBF",
    alignSelf: "center",
    justifyContent: "center",
  },
  row2: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  buttonText: {
    fontSize: 12,
    color: "white",
    alignSelf: "center",
    lineHeight: 15,
  },
  button: {
    paddingLeft: "5%",
    paddingRight: "5%",
    borderRadius: 10,
    backgroundColor: "#47AB7F",
    marginRight: "5%",
    justifyContent: "center",
    paddingTop: 0,
    paddingBottom: 0,
    marginTop: "1%",
    alignItems: "center",
  },
  input: { width: "70%" },
  row: {
    flexDirection: "row",
    width: "100%",
    marginTop: 10,
    alignItems: "center",
    justifyContent: "space-between",
  },
  title: {
    fontWeight: "600",
    lineHeight: 30,
    fontSize: 16,
  },
  secondaryTitle: {
    marginTop: 5,
    fontWeight: "600",

    fontSize: 14,
    marginBottom: 30,
    marginLeft: 15,
  },
  icon: {
    fontSize: 30,
    color: "green",
    fontWeight: "600",

    marginRight: 20,
    alignSelf: "center",
    marginBottom: "5%",
  },
  box: {
    padding: "5%",
  },
  container: {
    backgroundColor: "white",
    flex: 1,
  },

  secondary: {
    marginTop: 0,
    fontWeight: "400",
    lineHeight: 25,
    fontSize: 16,
    marginBottom: 0,
  },
});
export default Skills;
