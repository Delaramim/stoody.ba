import React, { useState } from "react";
import Steps from "../../components/Steps";
import { useForm } from "react-hook-form";
import { View, StyleSheet, Text, Modal, ScrollView } from "react-native";
import { Button, Icon } from "native-base";

import Footer from "../../components/FooterRegister";
import { FormInput } from "../../components";
import { useUserContext } from "../../context";

const Education = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const { control, handleSubmit, errors } = useForm();

  const { educations, setEducations } = useUserContext();

  const onSubmit = (data) => {
    // console.log("DATA", data);
    if (data.school != "") setEducations((oldData) => [...oldData, data]);

    setModalVisible(false);
  };
  const handleDelete = (education) => {
    console.log(education);
    setEducations(educations.filter((i) => i != education));
  };

  return (
    <View style={styles.container}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.modalHeader}>
              <Icon
                onPress={() => setModalVisible(!modalVisible)}
                style={styles.closeIcon}
                active
                name="close-outline"
                type="Ionicons"
              />
              <Text style={styles.modalTitle}>Education</Text>
            </View>
            <View style={styles.box}>
              <FormInput
                required
                placeholder="Ex: Northwestern University"
                name={"school"}
                control={control}
                text={"School"}
              />

              <FormInput
                required
                placeholder="Ex: Computer Science"
                name={"study"}
                control={control}
                text={"Area of study (Optional)"}
              />

              <FormInput
                required
                placeholder="Ex: Bachelor's"
                name={"degree"}
                control={control}
                text={"Degree (Optional)"}
              />
            </View>

            <View style={styles.footer}>
              <Footer
                nextPage={"Experience"}
                text={"Save"}
                onPress={handleSubmit(onSubmit)}
                backgroundColor={"#34C787"}
                color={"white"}
              ></Footer>
            </View>
          </View>
        </View>
      </Modal>
      <Steps percentage={"60%"} />
      <View style={styles.box}>
        <Text style={styles.title}>Add your education</Text>
        <Text style={styles.secondary}>
          Add the schools you attended, areas of study, and degrees earned
        </Text>

        <Button style={styles.button} onPress={() => setModalVisible(true)}>
          <Icon style={styles.icon} active name="add-outline" type="Ionicons" />
          <Text style={styles.buttonText}>Add Education</Text>
        </Button>
        <ScrollView>
          {educations?.map((education, index) => {
            if (educations.length != 0)
              return (
                <View style={styles.row} key={index}>
                  <View key={index} style={styles.newEducation}>
                    <Text style={styles.secondaryTitle}>
                      {education.school}
                    </Text>
                    <Text style={styles.secondary}>{education.study}</Text>
                    <Text style={styles.dateText}>{education.degree}</Text>
                  </View>
                  <Icon
                    name={"close-outline"}
                    type="Ionicons"
                    style={styles.trash}
                    onPress={() => handleDelete(education)}
                  />
                </View>
              );
          })}
        </ScrollView>
      </View>
      <Footer
        nextPage={"Experience"}
        text={"Next"}
        onPress={() => navigation.navigate("Experience")}
        backgroundColor={"#34C787"}
        color={"white"}
      ></Footer>
    </View>
  );
};

const styles = StyleSheet.create({
  trash: {
    fontSize: 20,
    color: "#C3BFBF",
    alignSelf: "center",
    justifyContent: "center",
    marginBottom: 35,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  newEducation: {
    marginTop: 10,
    marginBottom: 10,
  },
  title: {
    fontWeight: "600",
    lineHeight: 30,
    fontSize: 16,
  },

  dateText: {
    marginTop: 0,
    fontWeight: "300",
    lineHeight: 20,
    fontSize: 16,
    marginBottom: 0,
  },
  buttonText1: {
    fontSize: 14,
    fontWeight: "600",
    color: "white",
  },
  next: {
    backgroundColor: "#622B15",
    borderRadius: 30,
    width: "70%",
    height: 50,
    alignSelf: "center",
    justifyContent: "center",
    fontSize: 14,
  },
  footer: {
    paddingBottom: 40,
    marginBottom: "10%",
  },
  descriptionInput: {
    justifyContent: "flex-start",
    alignItems: "flex-start",
    borderRadius: 5,
    paddingLeft: 20,
    paddingRight: 20,
    height: 100,
    marginBottom: 25,
  },
  input: {
    borderRadius: 5,
    paddingLeft: 20,
    paddingRight: 20,
    height: 43,
    marginBottom: 25,
  },
  secondaryTitle: {
    marginTop: 5,
    fontWeight: "600",
    lineHeight: 20,
    fontSize: 14,
    marginBottom: 0,
  },
  closeIcon: {
    fontSize: 30,
    color: "white",
    fontWeight: "600",
    marginRight: "30%",
  },

  modalHeader: {
    padding: 20,
    justifyContent: "center",
    width: "100%",
    height: 65,
    flexDirection: "row",
    backgroundColor: "#5159DA",
    borderTopEndRadius: 35,
    borderTopStartRadius: 35,
    justifyContent: "flex-start",
  },
  centeredView: {
    flex: 1,
    marginTop: 22,
  },
  modalView: {
    marginTop: "8%",
    marginBottom: 0,
    backgroundColor: "white",
    borderRadius: 35,

    shadowColor: "#000",
    width: "100%",
    height: "100%",
    // paddingLeft: 35,
    // paddingRight: 35,
    bottom: 0,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },

  icon: {
    fontSize: 20,
    color: "#34C787",
    fontWeight: "600",
    marginLeft: 5,
    marginRight: 5,
  },
  box: {
    padding: "5%",
    height: "83%",
  },
  container: {
    backgroundColor: "white",
    flex: 1,
  },
  modalTitle: {
    fontWeight: "600",
    lineHeight: 30,
    fontSize: 18,
    color: "#FFFFFF",
  },
  secondary: {
    marginTop: 0,
    fontWeight: "400",
    lineHeight: 25,
    fontSize: 16,
    marginBottom: 0,
  },
  button: {
    marginTop: 20,
    backgroundColor: "white",
    borderRadius: 10,
    width: "100%",
    height: 45,
    alignSelf: "center",
    justifyContent: "center",
    fontSize: 14,
    borderWidth: 0.2,
    borderColor: "#34C787",
  },
  buttonText: { fontSize: 16, fontWeight: "600", color: "#34C787" },
});

export default Education;
