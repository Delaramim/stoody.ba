import React, { useEffect } from "react";
import { Image } from "react-native";
import { View, StyleSheet, ScrollView } from "react-native";
import { useForm, Controller } from "react-hook-form";
import { Text, Button, Form, Item, Input, Label, Icon } from "native-base";
import { FooterRegister, FormIconInput, RadioInput } from "../../components";
import { useUserContext, useAuthContext } from "../../context";

const RegisterScreen = ({ navigation }) => {
  const { control, handleSubmit, errors } = useForm();
  const { setFirstName, setLastName, setUsername } = useUserContext();
  const { doSignUp, user, error, setError } = useAuthContext();

  useEffect(() => {
    setError(" ");
  }, []);
  const onSubmit = (data) => {
    // console.log(data.email);
    // setUsername(data.email);

    setUsername(data.username);
    setLastName(data.lastName);
    setFirstName(data.firstName);

    doSignUp({ email: data.email, password: data.password });
    if (user) navigation.navigate("Register");
  };
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.topTextContainer}>
          <Text style={styles.title}>Set up your free account</Text>
        </View>

        <Form>
          <FormIconInput
            required
            placeholder={"First name"}
            iconName={"person-outline"}
            name={"firstName"}
            control={control}
            autoCompleteType={"name"}
          />

          <FormIconInput
            required
            placeholder={"Last name"}
            iconName={"person-outline"}
            name={"lastName"}
            control={control}
            autoCompleteType={"name"}
          />
          <FormIconInput
            required
            placeholder={"E-mail"}
            iconName={"mail-outline"}
            name={"email"}
            control={control}
            autoCompleteType={"email"}
            autoCapitalize={"none"}
          />
          <FormIconInput
            required
            placeholder={"Create password"}
            iconName={"lock-closed-outline"}
            name={"password"}
            control={control}
            secureTextEntry={true}
            autoCompleteType={"password"}
            autoCapitalize={"none"}
          />

          <RadioInput />

          {/* <FormIconInput
            placeholder={"Username"}
            iconName={"person"}
            name={"username"}
            control={control}
          /> */}
        </Form>
        <Text style={styles.error}>{error}</Text>
      </ScrollView>
      <FooterRegister
        nextPage={"Experience"}
        text={"Next"}
        onPress={handleSubmit(onSubmit)}
        backgroundColor={"#34C787"}
        color={"white"}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  logo: { width: 225, height: 66 },
  error: {
    fontSize: 12,
    color: "red",
    alignSelf: "center",
    marginBottom: 20,
  },

  buttonText: { fontSize: 14, fontWeight: "500" },
  login: {
    backgroundColor: "#622B15",
    borderRadius: 30,
    width: "80%",
    height: 50,
    alignSelf: "center",
    justifyContent: "center",
    fontSize: 14,
  },
  form: {
    width: "80%",
    marginTop: 30,
    marginBottom: 80,
    alignSelf: "center",
  },
  container: {
    paddingTop: "10%",
    flex: 1,
    backgroundColor: "#FFFDF9",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontWeight: "700",
    alignSelf: "center",
    lineHeight: 30,
    fontSize: 20,
    textAlign: "center",
  },
  topTextContainer: {
    // marginTop: 10,
    width: "80%",
    justifyContent: "center",
    alignSelf: "center",
    marginBottom: 40,
  },
  secondaryText: {
    fontSize: 14,
    alignSelf: "center",
    marginTop: 15,
  },
  inputBox: {
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0.3,
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 25,
  },
  inputStyle: {
    borderWidth: 0,
    padding: 30,
  },
  icon: {
    fontSize: 14,
    color: "#656565",
  },
  inputText: { fontSize: 14 },
});

export default RegisterScreen;
