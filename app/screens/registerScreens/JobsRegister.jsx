import React from "react";
import { View, StyleSheet } from "react-native";
import { useForm, Controller } from "react-hook-form";
import { useUserContext } from "../../context/userContext";

import Steps from "../../components/Steps";
import FormInput from "../../components/FormInput";
import Footer from "../../components/FooterRegister";

import { FormLongInput } from "../../components/";

const JobRegister = ({ navigation }) => {
  console.log("hello");
  const { setJobTitle, setManiServices, setBio, isStudent } = useUserContext();
  const { control, handleSubmit, errors } = useForm();

  const onSubmit = (data) => {
    setJobTitle(data.workMain);
    setManiServices(data.workSecondary);
    setBio(data.bio);
    if (isStudent) navigation.navigate("Skills");
    if (!isStudent) navigation.navigate("Complete");
  };
  return (
    <View style={styles.container}>
      <Steps percentage={isStudent ? "20%" : "100%"} />

      <View style={styles.box}>
        <FormLongInput
          text={"Tell us about yourself"}
          placeholder={
            "Ex: I began my career in retail management, but a few years ago.."
          }
          name={"bio"}
          control={control}
        />

        <FormInput
          text={"Tell us about the work you do"}
          placeholder={"Ex: Developer"}
          name={"workMain"}
          control={control}
        />

        <FormInput
          text="What is the main service you provide?"
          placeholder="Ex: React"
          name={"workSecondary"}
          control={control}
        />
      </View>
      <Footer
        text={"Next"}
        onPress={handleSubmit(onSubmit)}
        backgroundColor={"#34C787"}
        color={"white"}
      ></Footer>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    marginTop: "5%",
    padding: "5%",
  },
  container: {
    backgroundColor: "#FFFDF9",
    flex: 1,
  },
  title: {
    fontWeight: "600",
    lineHeight: 20,
    fontSize: 16,
  },
  secondary: {
    marginTop: 5,
    fontWeight: "600",
    lineHeight: 30,
    fontSize: 14,
    marginBottom: 5,
  },
  button: {
    backgroundColor: "#622B15",
    borderRadius: 30,
    width: "80%",
    height: 50,
    alignSelf: "center",
    justifyContent: "center",
    fontSize: 14,
  },
  buttonText: { fontSize: 14, fontWeight: "500" },
});
export default JobRegister;
