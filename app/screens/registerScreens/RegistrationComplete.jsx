import React, { useState, useEffect } from "react";
import Steps from "../../components/Steps";
import { useForm, Controller } from "react-hook-form";
import { View, StyleSheet, Text, Image } from "react-native";
import { addStudent, addEmployer } from "../../api/UserApi";
import { fetchUser } from "../../api/UserApi";
import { useAuthContext, useUserContext } from "../../context";
import Footer from "../../components/FooterRegister";

const RegistrationComplete = ({ navigation }) => {
  const { control, handleSubmit, errors } = useForm();
  // console.log(isStudent);

  const { student, setStudent, user } = useAuthContext();

  const getStudent = (student) => {
    setStudent(student);
  };

  useEffect(() => {
    fetchUser(user.uid, getStudent);
  }, []);

  const {
    firstName,
    lastName,
    skills,
    educations,
    bio,
    mainServices,
    jobTitle,
    experiences,
    languages,
    isStudent,
  } = useUserContext();

  const data = {
    id: user.uid,
    firstName: firstName,
    lastName: lastName,
    skills: skills,
    bio: bio,
    mainServices: mainServices,
    jobTitle: jobTitle,
    educations: educations,
    experiences: experiences,
    languages: languages,
    isStudent: isStudent,

    // username: username,}
  };

  const onSubmit = () => {
    setStudent("someone");
    if (isStudent) {
      addStudent(data);
    }
    if (!isStudent) addEmployer(data);
  };
  return (
    <View style={styles.container}>
      <View style={styles.box}>
        <Text style={styles.header}>Welcome to Stoody!</Text>
        <Image
          style={styles.img}
          source={require("../../assets/complete.png")}
        />
      </View>
      <Footer
        text={"Explore"}
        onPress={() => onSubmit()}
        backgroundColor={"#34C787"}
        color={"white"}
      ></Footer>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    fontWeight: "600",
    lineHeight: 30,
    fontSize: 30,
    marginBottom: "20%",
    color: "#4356C9",
  },

  img: { width: 327.46, height: 335, alignSelf: "center" },
  box: {
    marginTop: "30%",
    padding: "10%",
  },
  container: {
    backgroundColor: "white",
    flex: 1,
  },
});
export default RegistrationComplete;
