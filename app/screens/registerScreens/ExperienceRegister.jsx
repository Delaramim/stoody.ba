import React, { useState } from "react";
import Steps from "../../components/Steps";
import { useForm, Controller } from "react-hook-form";
import {
  View,
  StyleSheet,
  Text,
  Modal,
  ScrollView,
  FlatList,
} from "react-native";
import { Button, Icon, Form } from "native-base";

import dates from "../../data/date.json";

import { Picker, FormInput, FooterRegister } from "../../components";
import { useUserContext } from "../../context";

const Experience = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const { control, handleSubmit, errors } = useForm();
  const { experiences, setExperiences } = useUserContext();
  const [from, setFrom] = useState(null);

  const onSubmit = (data) => {
    if (
      data.company != "" &&
      data.from != "" &&
      data.location != "" &&
      data.title != "" &&
      data.to != ""
    )
      setExperiences((oldData) => [...oldData, data]);
    setModalVisible(false);
    setExperience(data.from, data);
  };

  const handleDelete = (experience) => {
    console.log(experience);
    setExperiences(experiences.filter((i) => i != experience));
  };

  return (
    <View style={styles.container}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.modalHeader}>
              <Icon
                onPress={() => setModalVisible(!modalVisible)}
                style={styles.closeIcon}
                active
                name="close-outline"
                type="Ionicons"
              />
              <Text style={styles.modalTitle}>Add employment</Text>
            </View>
            <View style={styles.box}>
              <Form>
                <FormInput
                  required
                  placeholder="Company name"
                  name={"company"}
                  control={control}
                  text={"Company"}
                />
                <FormInput
                  required
                  placeholder="City"
                  name={"location"}
                  control={control}
                  text={"Location"}
                />
                <FormInput
                  required
                  placeholder="Job title"
                  name={"title"}
                  control={control}
                  text={"Title"}
                />
                <Text style={styles.secondaryTitle}>Period</Text>
                <Picker
                  required
                  data={dates.from}
                  title={"From"}
                  control={control}
                  name={"from"}
                />

                <Picker
                  required
                  data={dates.till}
                  title={"Till"}
                  control={control}
                  name={"to"}
                />
              </Form>
            </View>
            <View style={styles.footer}>
              <FooterRegister
                nextPage={"Experience"}
                text={"Save"}
                onPress={handleSubmit(onSubmit)}
                backgroundColor={"#34C787"}
                color={"white"}
              />
            </View>
          </View>
        </View>
      </Modal>
      <Steps percentage={"80%"}></Steps>
      <View style={styles.box}>
        <Text style={styles.title}>Add your past work experience</Text>
        <Text style={styles.secondary}>
          Build your credibility by showcasing the projects or jobs you have
          completed.
        </Text>

        <Button style={styles.button} onPress={() => setModalVisible(true)}>
          <Icon style={styles.icon} active name="add-outline" type="Ionicons" />
          <Text style={styles.buttonText}>Add Employment</Text>
        </Button>
        <ScrollView>
          {experiences.map((experience, index) => {
            return (
              <View style={styles.row} key={index}>
                <View style={styles.newExperience} key={index}>
                  <Text style={styles.secondaryTitle}>
                    {experience.company}
                  </Text>
                  <Text style={styles.secondary}>{experience.title}</Text>
                  <Text style={styles.dateText}>
                    {experience.from + " - " + experience.to}
                  </Text>
                </View>
                <Icon
                  name={"close-outline"}
                  type="Ionicons"
                  style={styles.trash}
                  onPress={() => handleDelete(experience)}
                />
              </View>
            );
          })}
        </ScrollView>
      </View>
      <FooterRegister
        text={"Next"}
        onPress={() => navigation.navigate("Languages")}
        backgroundColor={"#34C787"}
        color={"white"}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  trash: {
    fontSize: 20,
    color: "#C3BFBF",
    alignSelf: "center",
    justifyContent: "center",
    marginBottom: 35,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  newExperience: {
    marginTop: 10,
    marginBottom: 10,
  },
  title: {
    fontWeight: "600",
    lineHeight: 30,
    fontSize: 16,
  },

  dateText: {
    marginTop: 0,
    fontWeight: "300",
    lineHeight: 20,
    fontSize: 16,
    marginBottom: 0,
  },
  footer: {
    paddingBottom: 40,
    marginBottom: "10%",
  },

  secondaryTitle: {
    marginTop: 5,
    fontWeight: "600",
    lineHeight: 20,
    fontSize: 14,
    marginBottom: 0,
  },
  closeIcon: {
    fontSize: 30,
    color: "white",
    fontWeight: "600",
    marginRight: "25%",
  },

  modalHeader: {
    padding: 20,
    justifyContent: "center",
    width: "100%",
    height: 65,
    flexDirection: "row",
    backgroundColor: "#5159DA",
    borderTopEndRadius: 35,
    borderTopStartRadius: 35,
    justifyContent: "flex-start",
  },
  centeredView: {
    flex: 1,
    marginTop: 22,
  },
  modalView: {
    marginTop: "8%",
    marginBottom: 0,
    backgroundColor: "white",
    borderRadius: 35,
    shadowColor: "#000",
    width: "100%",
    height: "100%",
    bottom: 0,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    flex: 1,
  },

  icon: {
    fontSize: 20,
    color: "#34C787",
    fontWeight: "600",
    marginLeft: 5,
    marginRight: 5,
  },
  box: {
    padding: "5%",
    height: "83%",
    flex: 1,
  },
  container: {
    backgroundColor: "white",
    flex: 1,
  },
  modalTitle: {
    fontWeight: "600",
    lineHeight: 30,
    fontSize: 18,
    color: "#FFFFFF",
  },
  secondary: {
    marginTop: 0,
    fontWeight: "400",
    lineHeight: 25,
    fontSize: 16,
    marginBottom: 0,
  },
  button: {
    marginTop: 30,
    backgroundColor: "white",
    borderRadius: 10,
    width: "100%",
    height: 45,
    alignSelf: "center",
    justifyContent: "center",
    fontSize: 14,
    borderWidth: 0.2,
    borderColor: "#34C787",
  },
  buttonText: { fontSize: 16, fontWeight: "600", color: "#34C787" },
});

export default Experience;
