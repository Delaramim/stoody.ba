import React, { useState } from "react";
import Steps from "../../components/Steps";
import { useForm, Controller } from "react-hook-form";
import {
  View,
  StyleSheet,
  Text,
  Pressable,
  Modal,
  ScrollView,
} from "react-native";
import { Button, Item, Icon, Form, Label } from "native-base";
import Footer from "../../components/FooterRegister";
import FormInput from "../../components/FormIconInput";

const Fee = () => {
  const { control, handleSubmit, errors } = useForm();

  const onSubmit = (data) => {
    setEducations((oldData) => [...oldData, data]);
    // console.log(data);
    setModalVisible(false);
  };
  return (
    <View style={styles.container}>
      <Steps percentage={"40%"}></Steps>
      <View style={styles.box}>
        <Form>
          <Text style={styles.title}>Hourly rate </Text>
          <Text>Total amount the client will see</Text>
          <View style={styles.row}>
            <View style={styles.input}>
              <FormInput
                placeholder={"00.00"}
                iconName={"wallet-outline"}
                name={"hourlyFee"}
                control={control}
              />
            </View>
            <View>
              <Text style={styles.secondaryTitle}>/hr</Text>
            </View>
            <View style={styles.divider}></View>
          </View>
        </Form>
      </View>
      <Footer text={"Next"}></Footer>
    </View>
  );
};

const styles = StyleSheet.create({
  divider: {
    borderBottomWidth: 0.3,
    borderColor: "#656565",
  },
  input: { width: "60%" },
  row: {
    flexDirection: "row",
    width: "100%",
    marginTop: 20,
    alignItems: "center",
    borderBottomWidth: 0.2,
    borderColor: "#656565",
  },
  title: {
    fontWeight: "600",
    lineHeight: 30,
    fontSize: 16,
  },
  secondaryTitle: {
    marginTop: 5,
    fontWeight: "600",

    fontSize: 14,
    marginBottom: 30,
    marginLeft: 15,
  },
  icon: {
    fontSize: 20,
    color: "#B56E52",
    fontWeight: "600",
    marginLeft: 5,
    marginRight: 5,
  },
  box: {
    padding: "5%",
  },
  container: {
    backgroundColor: "white",
    flex: 1,
  },

  secondary: {
    marginTop: 0,
    fontWeight: "400",
    lineHeight: 25,
    fontSize: 16,
    marginBottom: 0,
  },
});
export default Fee;
