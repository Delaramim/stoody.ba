import React, { useState } from "react";
import Steps from "../../components/Steps";
import { useForm, Controller } from "react-hook-form";
import { View, StyleSheet, Text } from "react-native";
import { Button, Item, Icon, Form, Label } from "native-base";
import Footer from "../../components/FooterRegister";
import FormInput from "../../components/FormIconInput";

import BulletedText from "../../components/BulletedText";
import { ScrollView } from "react-native-gesture-handler";
import { useAuthContext } from "../../context";
import { EditLanguages } from "../../api/UserApi";

const Skills = ({ navigation }) => {
  const { control, handleSubmit, errors } = useForm();
  const { student } = useAuthContext();

  const [languages, setLanguages] = useState(student?.languages);

  const onSubmit = (data) => {
    if (data.language != "") {
      setLanguages((oldData) => [...oldData, data]);
    }
  };

  const handleDelete = (language) => {
    setLanguages(languages.filter((i) => i != language));
  };

  const handleSave = () => {
    const id = student?.id;
    EditLanguages({ id, languages });
    navigation.goBack();
  };
  return (
    <View style={styles.container}>
      <Steps percentage={"100%"} />
      <View style={styles.box}>
        <Form>
          <Text style={styles.title}>Input the languages that you speak</Text>
          <View style={styles.row}>
            <View style={styles.input}>
              <FormInput
                placeholder="Add language"
                name={"language"}
                control={control}
                text={"Title"}
                clearButtonMode="always"
              />
            </View>
            <Button style={styles.button} onPress={handleSubmit(onSubmit)}>
              <Text style={styles.buttonText}>Add</Text>
            </Button>
          </View>
          <ScrollView>
            {languages.map((language, index) => {
              return (
                <View style={styles.row2} key={index}>
                  <BulletedText key={index} languageName={language.language} />
                  <Icon
                    name={"close-outline"}
                    type="Ionicons"
                    style={styles.trash}
                    onPress={() => handleDelete(language)}
                    clearButtonMode="always"
                  />
                </View>
              );
            })}
          </ScrollView>
        </Form>
      </View>
      <Footer
        text={"Save"}
        onPress={() => handleSave()}
        backgroundColor={"#34C787"}
        color={"white"}
      ></Footer>
    </View>
  );
};

const styles = StyleSheet.create({
  trash: {
    fontSize: 20,
    color: "#C3BFBF",
    alignSelf: "center",
    justifyContent: "center",
  },
  row2: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  buttonText: {
    fontSize: 12,
    color: "white",
    alignSelf: "center",
    lineHeight: 15,
  },
  button: {
    paddingLeft: "5%",
    paddingRight: "5%",
    borderRadius: 10,
    backgroundColor: "#47AB7F",
    marginRight: "5%",
    justifyContent: "center",
    paddingTop: 0,
    paddingBottom: 0,
    marginTop: "1%",
    alignItems: "center",
  },
  input: { width: "70%" },
  row: {
    flexDirection: "row",
    width: "100%",
    marginTop: 20,
    alignItems: "center",
  },
  title: {
    fontWeight: "600",
    lineHeight: 30,
    fontSize: 16,
  },
  secondaryTitle: {
    marginTop: 5,
    fontWeight: "600",

    fontSize: 14,
    marginBottom: 30,
    marginLeft: 15,
  },
  icon: {
    fontSize: 30,
    color: "green",
    fontWeight: "600",

    marginRight: 20,
    alignSelf: "center",
    marginBottom: "5%",
  },
  box: {
    padding: "5%",
  },
  container: {
    backgroundColor: "white",
    flex: 1,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  secondary: {
    marginTop: 0,
    fontWeight: "400",
    lineHeight: 25,
    fontSize: 16,
    marginBottom: 0,
  },
});
export default Skills;
