import React from "react";
import { View, StyleSheet } from "react-native";
import { useForm } from "react-hook-form";
import { useUserContext, useAuthContext } from "../../context";

import Steps from "../../components/Steps";
import FormInput from "../../components/FormInput";
import Footer from "../../components/FooterRegister";
import { EditBio } from "../../api/UserApi";

import { FormLongInput } from "../../components";

const JobRegister = ({ navigation }) => {
  const { student } = useAuthContext();
  const { control, handleSubmit, errors } = useForm();

  const onSubmit = (data) => {
    // console.log("NEW DATA", data);
    const id = student?.id;
    if (data) {
      EditBio({ id, data });
      navigation.goBack();
    }
  };
  return (
    <View style={styles.container}>
      <Steps percentage={"100%"} />

      <View style={styles.box}>
        <FormLongInput
          text={"Tell us about yourself"}
          placeholder={student?.bio}
          name={"bio"}
          control={control}
          defaultValue={student?.bio}
        />

        <FormInput
          text={"Tell us about the work you do"}
          placeholder={student?.jobTitle}
          name={"workMain"}
          control={control}
          defaultValue={student?.jobTitle}
        />

        <FormInput
          text="What is the main service you provide?"
          placeholder={student?.mainServices}
          name={"workSecondary"}
          control={control}
          defaultValue={student?.mainServices}
        />
      </View>
      <Footer
        text={"Save"}
        onPress={handleSubmit(onSubmit)}
        backgroundColor={"#34C787"}
        color={"white"}
      ></Footer>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    marginTop: "5%",
    padding: "5%",
  },
  container: {
    backgroundColor: "#FFFDF9",
    flex: 1,
  },
  title: {
    fontWeight: "600",
    lineHeight: 20,
    fontSize: 16,
  },
  secondary: {
    marginTop: 5,
    fontWeight: "600",
    lineHeight: 30,
    fontSize: 14,
    marginBottom: 5,
  },
  button: {
    backgroundColor: "#622B15",
    borderRadius: 30,
    width: "80%",
    height: 50,
    alignSelf: "center",
    justifyContent: "center",
    fontSize: 14,
  },
  buttonText: { fontSize: 14, fontWeight: "500" },
});
export default JobRegister;
