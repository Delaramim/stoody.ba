import React, { useState } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { FormInput, FormLongInput, FormIconInput } from "../../components";
import { useForm, Controller } from "react-hook-form";
import Requirement from "../../components/Requirement";
import Footer from "../../components/FooterRegister";
import { useTaskContext, useAuthContext } from "../../context";
import { EditTask } from "../../api/TaskApi";

import { Button, Content, Icon } from "native-base";

const CreateTask = ({ navigation, route }) => {
  const task = route.params.task;

  const { control, handleSubmit, errors } = useForm();
  const { user, student } = useAuthContext();

  const [requirements, setRequirements] = useState(task.requirements);

  const id = task.id;
  const onSubmit = (data) => {
    EditTask({ id, data, requirements });
    navigation.goBack();
  };
  const onAdd = (data) => {
    const requirement = data.requirement;
    if (data.requirements != "") {
      setRequirements((oldData) => [...oldData, requirement]);
    }
  };

  const handleDelete = (requirement) => {
    setRequirements(requirements.filter((i) => i != requirement));
  };

  return (
    <View style={styles.container}>
      <Content style={styles.box}>
        <FormInput
          text={"Task title"}
          placeholder="Ex: Logo creation"
          name={"taskName"}
          control={control}
          defaultValue={task?.name}
        />
        <FormInput
          text={"Compensation"}
          placeholder="200.00 KM"
          name={"compensation"}
          control={control}
          defaultValue={task?.compensation}
        />
        <FormInput
          text={"Location"}
          placeholder="Ex: Sarajevo"
          name={"location"}
          control={control}
          defaultValue={task?.location}
        />

        <FormLongInput
          text={"Task description"}
          placeholder="Description..."
          name={"taskDescription"}
          control={control}
          defaultValue={task?.description}
        />

        <View>
          <Text style={styles.title}>Task requirements </Text>
          <View style={styles.row}>
            <Button style={styles.button} onPress={handleSubmit(onAdd)}>
              <Text style={styles.buttonText}>Add</Text>
            </Button>

            <View style={styles.input}>
              <FormIconInput
                placeholder="Add requirement"
                name={"requirement"}
                control={control}
                text={"Title"}
              />
            </View>
          </View>
          <View>
            {requirements?.map((requirement, index) => {
              console.log(requirement);
              return (
                <View style={[styles.row, { marginBottom: 200 }]} key={index}>
                  <Requirement key={index} text={requirement} />
                  <Icon
                    name={"close-outline"}
                    type="Ionicons"
                    style={styles.trash}
                    onPress={() => handleDelete(requirement)}
                  />
                </View>
              );
            })}
          </View>
        </View>
      </Content>
      <Footer
        // nextPage={"Experience"}
        text={"Save"}
        onPress={handleSubmit(onSubmit)}
        backgroundColor={"#34C787"}
        color={"white"}
      ></Footer>
    </View>
  );
};

const styles = StyleSheet.create({
  trash: {
    fontSize: 20,
    color: "#C3BFBF",
    alignSelf: "center",
    justifyContent: "center",
    marginRight: "30%",
    marginTop: 20,
  },

  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    paddingBottom: 50,
  },
  box: {
    marginTop: 20,
    width: "100%",
  },
  buttonText: {
    fontSize: 12,
    color: "white",
    alignSelf: "center",
    lineHeight: 15,
  },
  button: {
    paddingLeft: "5%",
    paddingRight: "5%",
    borderRadius: 10,
    backgroundColor: "#47AB7F",
    marginRight: "5%",
    justifyContent: "center",
    paddingTop: 0,
    paddingBottom: 0,
    marginTop: "1%",
    alignItems: "center",
  },
  input: { width: "60%" },
  row: {
    flexDirection: "row",
    width: "100%",
    marginTop: 5,
    alignItems: "center",
  },
  title: {
    fontWeight: "600",
    lineHeight: 25,
    fontSize: 16,
  },
  secondaryTitle: {
    marginTop: 5,
    fontWeight: "600",

    fontSize: 14,
    marginBottom: 30,
    marginLeft: 15,
  },
  icon: {
    fontSize: 30,
    color: "green",
    fontWeight: "600",

    marginRight: 20,
    alignSelf: "center",
    marginBottom: "5%",
  },

  secondary: {
    marginTop: 0,
    fontWeight: "400",
    lineHeight: 25,
    fontSize: 16,
    marginBottom: 0,
  },
});

export default CreateTask;
