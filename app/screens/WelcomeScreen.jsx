import React from "react";
import { ImageBackground, View, StyleSheet, Image } from "react-native";
import { Text, Button } from "native-base";
import MyButton from "../components/Button";

const WelcomeScreen = ({ navigation }) => {
  return (
    <View style={styles.background}>
      <ImageBackground
        style={styles.backgroundImage}
        source={require("../assets/welcomeBG.png")}
      >
        <View style={styles.content}>
          <View>
            <Image
              style={styles.logo}
              source={require("../assets/welcomeLogo.png")}
            />
          </View>

          <View style={styles.buttons}>
            <MyButton
              width={300}
              onPress={() => navigation.navigate("Login")}
              title={"Login"}
              backgroundColor={"#34C787"}
              color={"white"}
            ></MyButton>
            <Text
              style={styles.register}
              onPress={() => navigation.navigate("Sign up")}
            >
              Register
            </Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  imageBox: { width: 241, height: 129 },

  background: {
    backgroundColor: "white",
    flex: 1,
  },
  backgroundImage: {
    width: "100%",
    height: "100%",
    alignContent: "center",
    justifyContent: "flex-end",
    flexDirection: "column",
  },

  logo: {
    width: 270,
    height: 103,
    alignSelf: "center",
  },
  content: {
    paddingBottom: "10%",
    alignSelf: "center",
    justifyContent: "space-between",
  },

  register: {
    alignSelf: "center",
    marginTop: 20,
    fontSize: 20,
    color: "white",
    textDecorationLine: "underline",
  },
  buttons: {
    marginTop: "40%",
    alignSelf: "center",
    width: "100%",
    marginBottom: "37%",
  },
});
export default WelcomeScreen;
