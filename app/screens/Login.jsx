import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { View, StyleSheet, Image } from "react-native";
import { Text, Form } from "native-base";
import { useAuthContext, useUserContext } from "../context";
import MyButton from "../components/Button";
import FormInput from "../components/FormInput2";

const LoginScreen = ({ navigation }) => {
  const { control, handleSubmit, errors } = useForm();
  const [skills, setSkills] = useState([]);
  const { doSignIn, error, setError } = useAuthContext();
  useEffect(() => {
    setError(" ");
  }, []);

  const onSubmit = (data) => {
    doSignIn({ email: data.email, password: data.password });
    setError(" ");
  };

  return (
    <View style={styles.container}>
      <Image style={styles.topImage} source={require("../assets/WTop.png")} />

      <View style={styles.box}>
        <Text style={styles.titleBig}>Welcome Back!</Text>

        <Form>
          <View style={styles.formInputs}>
            <FormInput
              placeholder="email"
              name={"email"}
              control={control}
              autoCapitalize={"none"}
            />
            <FormInput
              placeholder="Password"
              name={"password"}
              control={control}
              secureTextEntry={true}
              autoCapitalize={"none"}
            />
            <Text style={styles.error}>{error}</Text>
          </View>

          <View style={styles.buttons}>
            <MyButton
              width={280}
              title={"Confirm"}
              backgroundColor={"#34C787"}
              onPress={handleSubmit(onSubmit)}
              color={"white"}
            />
            <Text
              style={styles.register}
              onPress={() => {
                navigation.navigate("Reset"), setError(" ");
              }}
            >
              Forgot Password?
            </Text>
          </View>
        </Form>
      </View>
      <Image
        style={styles.bottomImage}
        source={require("../assets/WBottom.png")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  formInputs: { marginTop: "10%", marginBottom: "10%" },
  error: {
    fontSize: 12,
    color: "red",
    alignSelf: "center",
  },
  box: {
    padding: "10%",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    marginTop: "30%",
  },
  titleBig: {
    fontSize: 30,
    fontWeight: "600",
    marginBottom: "15%",
  },

  bottomImage: {
    marginTop: "90%",
    height: 78.68,
    width: "100%",
    position: "absolute",
    bottom: 0,
  },
  topImage: {
    height: 110,
    width: "100%",
    position: "absolute",
    top: 0,
    zIndex: 1,
  },

  buttons: {
    alignSelf: "center",
    width: "100%",
    marginBottom: "35%",
    marginTop: "5%",
  },
  register: {
    alignSelf: "center",
    marginTop: 20,
    fontSize: 20,
    color: "#34C787",
    textDecorationLine: "underline",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
  },
});

export default LoginScreen;
