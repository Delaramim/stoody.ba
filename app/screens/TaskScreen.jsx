import React, { useState, useEffect } from "react";
import { Rating, Avatar } from "react-native-elements";
import {
  StyleSheet,
  Text,
  ImageBackground,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { Icon } from "native-base";
import Badge from "../components/Badge";
import Requirement from "../components/Requirement";
import Tag from "../components/TaskTags";
import Footer from "../components/FooterRegister";
import { applyTask, completeTask } from "../api/TaskApi";
import { useAuthContext } from "../context";
import EditAndDelete from "../components/TaskEditAndDelete";
import { sendPushNotification } from "../notifications/notifications";

import { fetchUser } from "../api/UserApi";

const Task = ({ route, navigation }) => {
  const { task } = route.params;

  const { student, expoPushToken } = useAuthContext();
  const { isStudent } = student;
  const [employer, setEmployer] = useState(null);
  const [employee, setEmployee] = useState(null);

  const {
    name,
    compensation,
    description,
    requirements,
    location,
    completed,
    assigned,
  } = task;

  console.log(assigned);
  const handleApply = () => {
    const id = task.id;
    const studentID = student.id;
    const title = "Application successful";
    const text = student.firstName + " applied for " + task.name;
    sendPushNotification({ expoPushToken, text, title });

    applyTask({ id, studentID });
    navigation.navigate("Home");
  };

  const handleComplete = () => {
    const id = task.id;
    completeTask(id);
    const title = "Task Complete!";
    const text = student.firstName + " completed " + task.name;
    sendPushNotification({ expoPushToken, text, title });
    navigation.navigate("Home");
  };

  const getEmployer = (employer) => {
    setEmployer(employer);
  };
  const getEmployee = (employee) => {
    setEmployee(employee);
  };

  useEffect(() => {
    fetchUser(task.employer, getEmployer);
    fetchUser(task.student, getEmployee);
  }, []);

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.box}>
          <Text style={styles.title}>{name}</Text>

          <Tag
            icon={"location-outline"}
            title={"Location"}
            input={location || "No location"}
          />
          <Tag
            icon={"cash-outline"}
            title={"Compensation"}
            input={compensation || "No compensation"}
          />
          <View style={{ marginTop: 15 }}>
            <View style={styles.row}>
              {completed && assigned ? (
                <Badge text={"Completed"} backgroundColor="#82E18C" />
              ) : (
                <Badge text={"In progress"} backgroundColor="#82B4E1" />
              )}
              {assigned ? (
                <Badge text={"Assigned"} backgroundColor="#82E18C" />
              ) : (
                <Badge text={"Not assigned"} backgroundColor="#82B4E1" />
              )}
            </View>
          </View>
        </View>

        {student?.id == task?.employer && !assigned ? (
          <View style={styles.box}>
            <EditAndDelete task={task ? task : null} navigation={navigation} />
          </View>
        ) : null}

        {student?.id !== task.employer ? (
          <TouchableOpacity
            style={styles.box}
            onPress={() => {
              const person = employer;
              navigation.navigate("OthersProfile", { person });
            }}
          >
            <Text style={styles.title}>Employer</Text>
            <View style={styles.alignColum}>
              <View style={styles.alignColum}>
                <Avatar
                  rounded
                  title={
                    employer?.firstName.charAt(0) + employer?.lastName.charAt(0)
                  }
                  containerStyle={{ backgroundColor: "#5D7BD7" }}
                />

                {employer ? (
                  <Text style={styles.username}>
                    {employer?.firstName} {employer?.lastName}
                  </Text>
                ) : null}
              </View>
            </View>
          </TouchableOpacity>
        ) : null}
        {student?.id !== task.student && assigned ? (
          <TouchableOpacity
            style={styles.box}
            onPress={() => {
              const person = employee;
              navigation.navigate("OthersProfile", { person });
            }}
          >
            <Text style={styles.title}>Assigned to</Text>
            <View style={styles.alignColum}>
              <View style={styles.alignColum}>
                <Avatar
                  rounded
                  title={
                    employee?.firstName.charAt(0) + employee?.lastName.charAt(0)
                  }
                  containerStyle={{ backgroundColor: "#C5B9F8" }}
                />

                <Text style={styles.username}>
                  {employee?.firstName} {employee?.lastName}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        ) : null}

        <View style={styles.box}>
          <Text style={styles.title}>Description</Text>
          <Text style={styles.text}>{description || "No description"}</Text>
        </View>

        <View style={styles.box}>
          <Text style={styles.title}>Requirements</Text>
          {requirements.length === 0 && <Text>No skills required.</Text>}
          {requirements.length > 0 &&
            requirements.map((requirement, index) => {
              return <Requirement text={requirement} key={index} />;
            })}
        </View>
        <View style={{ marginTop: "50%" }} />
      </ScrollView>

      {isStudent && !task.assigned ? (
        <Footer
          nextPage={"Experience"}
          text={"Apply"}
          onPress={() => handleApply()}
          backgroundColor={"#34C787"}
          color={"white"}
        />
      ) : null}

      {task.assigned && !task.completed && task.student == student.id ? (
        <Footer
          nextPage={"Experience"}
          text={"Completed"}
          onPress={() => handleComplete()}
          backgroundColor={"#34C787"}
          color={"white"}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  avatar: {
    backgroundColor: "#C2CFF4",
  },
  verticalLine: {
    height: "100%",
    width: 1,
    backgroundColor: "#909090",
    marginLeft: 50,
    marginRight: 50,
  },
  row: { flexDirection: "row" },
  icon: {
    color: "black",
    fontSize: 20,
    alignSelf: "center",
    marginRight: 7,
  },
  taskImage: {
    height: "100%",
    width: "100%",
    zIndex: 0,
  },
  container: {
    flex: 1,
    backgroundColor: "#F3F5FB",
  },
  overlay: {
    zIndex: 1,
    flex: 1,
    position: "absolute",
    left: 0,
    top: 0,
    opacity: 0.25,
    backgroundColor: "black",
    width: "100%",
    height: "100%",
  },
  imageContainer: {
    width: "100%",
    height: "25%",
  },
  text: {
    lineHeight: 20,
    fontSize: 14,
  },
  box: {
    padding: (17, 25),
    borderBottomWidth: 0.3,
    borderTopWidth: 0.3,
    backgroundColor: "white",
    borderColor: "#D6D8F0",
    marginBottom: 5,
  },
  alignColum: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  title: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 15,
  },
  date: {
    fontWeight: "400",
    fontSize: 10,
    color: "#808080",
  },
  username: {
    fontSize: 14,
    fontWeight: "500",
    marginLeft: 10,
  },
  ratingText: {
    color: "#808080",
    fontWeight: "500",
    lineHeight: 20,
    fontSize: 14,
    marginLeft: 10,
  },
});

export default Task;
