import React, { useEffect, useState } from "react";
import { View, StyleSheet } from "react-native";
import { Content, Text, Icon, Button } from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import TaskCard from "../components/TaskCard";
import * as firebase from "firebase";
import { useAuthContext, useTaskContext } from "../context";
import { addToken, fetchUser } from "../api/UserApi";
import Search from "../components/Search";
import { sendPushNotification } from "../notifications/notifications";

const HomeScreen = ({ navigation }) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const text = "New tasks with your needed skills have been added";
  const title = "New tasks";
  const { tasks, setTasks, isSearching, searchedTasks, setIsSearching } =
    useTaskContext();

  const { student, setStudent, user, expoPushToken } = useAuthContext();

  const getStudent = (student) => {
    setStudent(student);
  };

  const getTasks = async () => {
    const unsubscribe = await firebase
      .firestore()
      .collection("Tasks")
      .where("assigned", "==", false)
      .onSnapshot(
        (snapshot) => {
          const taskList = snapshot.docs.map((doc) => ({
            id: doc.id,
            name: doc.data().name,
            description: doc.data().description,
            compensation: doc.data().compensation,
            requirements: doc.data().requirements,
            location: doc.data().location,
            employer: doc.data().employer,
            student: doc.data().student,
            assigned: doc.data().assigned,
            completed: doc.data().completed,
            // createdAt: doc.data().createdAt,
            assigned: doc.data().assigned,
          }));
          setTasks(taskList);

          setLoading(false);
          if (
            expoPushToken
            // &&
            // taskList.requirements.some((ai) => student.skills.includes(ai))
          )
            sendPushNotification({ expoPushToken, text, title });
        },
        () => {
          setError(true);
          setLoading(false);
        }
      );
    fetchUser(user.uid, getStudent);

    return () => unsubscribe();
  };

  useEffect(() => {
    getTasks();
  }, []); // <----- empty dependency means it only runs on first mount

  return (
    <View style={styles.background}>
      <Content>
        <Search />
        {!isSearching
          ? tasks.map((task) => {
              return (
                <TaskCard navigation={navigation} task={task} key={task.id} />
              );
            })
          : searchedTasks.map((task) => {
              return (
                <TaskCard navigation={navigation} task={task} key={task.id} />
              );
            })}
      </Content>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    backgroundColor: "#F3F5FB",
    flex: 1,
  },
  card: {
    marginTop: 15,
    alignSelf: "center",
    width: "95%",
    borderRadius: 15,
    shadowOffset: { width: 0, height: 10 },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    shadowColor: "rgba(0, 0, 0, 0.05)",
    borderLeftWidth: 10,
    borderLeftColor: "#622B15",
    borderRightWidth: 10,
    borderRightColor: "white",
  },

  title: {
    fontWeight: "bold",
    marginBottom: 8,
  },
  date: {
    fontWeight: "400",
    fontSize: 10,
    color: "#808080",
  },
  dueDate: {
    backgroundColor: "#B56E52",
    fontSize: 10,
    borderRadius: 9,
    alignSelf: "flex-end",
  },
  icon: {
    fontSize: 20,
    color: "white",
    fontWeight: "600",
    marginLeft: 5,
    marginRight: 5,
  },
  button: {
    marginTop: 20,
    backgroundColor: "#34C787",
    borderRadius: 10,
    width: "90%",
    height: 45,
    alignSelf: "center",
    justifyContent: "center",
    fontSize: 14,
    borderWidth: 0.2,
    borderColor: "#34C787",
  },
  buttonText: { fontSize: 16, fontWeight: "600", color: "white" },
});

export default HomeScreen;
