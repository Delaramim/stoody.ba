import EducationRegister from "./registerScreens/EducationRegister";
import ExperienceRegister from "./registerScreens/ExperienceRegister";
import FeeRegister from "./registerScreens/FeeRegister";
import JobRegister from "./registerScreens/JobsRegister";
import LanguageRegister from "./registerScreens/LanguageRegister";
import SignUpScreen from "./registerScreens/SignUpScreen";
import RegistrationComplete from "./registerScreens/RegistrationComplete";
import SkillsRegister from "./registerScreens/SkillsRegister";
import CreateTask from "./CreateTask";
import HomeScreen from "./Home";
import Login from "./Login";
import MyTasks from "./MyTasks";
import StudentProfile from "./StudentProfile";
import TaskScreen from "./TaskScreen";
import WelcomeScreen from "./WelcomeScreen";
import OthersProfile from "./OthersProfile";
import BioEdit from "./editScreens/BioEdit";
import ExperienceEdit from "./editScreens/ExperienceEdit";
import EducationEdit from "./editScreens/EducationEdit";
import SkillsEdit from "./editScreens/SkillsEdit";
import LanguageEdit from "./editScreens/LanguageEdit";
import TaskEdit from "./editScreens/TaskEdit";
import PasswordReset from "./PasswordReset";
export {
  EducationRegister,
  ExperienceRegister,
  FeeRegister,
  JobRegister,
  LanguageRegister,
  SignUpScreen,
  RegistrationComplete,
  SkillsRegister,
  CreateTask,
  HomeScreen,
  Login,
  MyTasks,
  StudentProfile,
  TaskScreen,
  WelcomeScreen,
  OthersProfile,
  BioEdit,
  ExperienceEdit,
  EducationEdit,
  SkillsEdit,
  LanguageEdit,
  TaskEdit,
  PasswordReset,
};
