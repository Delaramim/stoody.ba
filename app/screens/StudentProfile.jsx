import React, { useState, useEffect } from "react";
import { Rating, Avatar } from "react-native-elements";
import { StyleSheet, ImageBackground, View, ScrollView } from "react-native";

import { Text } from "native-base";
import Divider from "../components/Divider";
import Tag from "../components/TaskTags";
import Experience from "../components/Experience";
import Skill from "../components/Requirement";
import Modal from "../components/ProfileModal";
import BulletedText from "../components/BulletedText";
import { useAuthContext, useUserContext } from "../context";
import { fetchUser } from "../api/UserApi";
import { Icon } from "native-base";

const StudentProfile = ({ navigation }) => {
  const { profileModal, edit } = useUserContext();

  const { student, setStudent, user } = useAuthContext();

  const getStudent = (student) => {
    setStudent(student);
  };

  useEffect(() => {
    fetchUser(user.uid, getStudent);
  }, []);

  if (student && student?.isStudent)
    return (
      <ScrollView style={styles.container} opacity={profileModal ? 0.5 : 1}>
        <View style={styles.imageContainer}>
          <ImageBackground
            style={styles.taskImage}
            source={require("../assets/profileBG.png")}
          />
        </View>
        <Avatar
          title
          rounded
          title={student?.firstName.charAt(0) + student?.lastName.charAt(0)}
          size={130}
          activeOpacity={0.7}
          containerStyle={styles.avatar}
        />

        <View style={styles.topBox}>
          <Text style={styles.username}>
            {student.firstName + " " + student.lastName}
          </Text>
          <Text style={styles.secondaryText}>{student.jobTitle}</Text>
          <Rating imageSize={20} size={20} style={{ paddingVertical: 10 }} />
          <Text style={styles.secondaryText}>4.8 (10)</Text>
        </View>
        <Divider />
        <View style={styles.box}>
          <View style={styles.row}>
            <Text style={styles.title}>Bio</Text>
            {edit ? (
              <Icon
                name={"create-outline"}
                type="Ionicons"
                style={styles.icon}
                onPress={() => navigation.navigate("Bio")}
              />
            ) : null}
          </View>
          <Text style={styles.text}>{student.bio}</Text>
        </View>
        <Divider />
        <View style={styles.box}>
          <View style={styles.row}>
            <Text style={styles.title}>Experience</Text>
            {edit ? (
              <Icon
                name={"create-outline"}
                type="Ionicons"
                style={styles.icon}
                onPress={() => navigation.navigate("Experience")}
              />
            ) : null}
          </View>
          {student.experiences.map((ex, index) => {
            return (
              <Experience
                key={index}
                job={ex.title}
                company={ex.company}
                from={ex.from}
                till={ex.to}
              />
            );
          })}
        </View>
        <Divider />
        <View style={styles.box}>
          <View style={styles.row}>
            <Text style={styles.title}>Education</Text>
            {edit ? (
              <Icon
                name={"create-outline"}
                type="Ionicons"
                style={styles.icon}
                onPress={() => navigation.navigate("Education")}
              />
            ) : null}
          </View>
          {student.educations.map((ed, index) => {
            return (
              <Experience
                key={index}
                job={ed.school}
                company={ed.degree}
                time={ed.study}
              />
            );
          })}
        </View>
        <Divider />
        <View style={styles.box}>
          <View style={styles.row}>
            <Text style={styles.title}>Skills</Text>
            {edit ? (
              <Icon
                name={"create-outline"}
                type="Ionicons"
                style={styles.icon}
                onPress={() => navigation.navigate("Skills")}
              />
            ) : null}
          </View>
          {student.skills.map((skill, index) => {
            return <Skill key={index} text={skill.skill} />;
          })}
        </View>
        <Divider />
        <View style={styles.box}>
          <View style={styles.row}>
            <Text style={styles.title}>Languages</Text>
            {edit ? (
              <Icon
                name={"create-outline"}
                type="Ionicons"
                style={styles.icon}
                onPress={() => navigation.navigate("Languages")}
              />
            ) : null}
          </View>

          {student.languages.map((lan, index) => {
            return <BulletedText key={index} languageName={lan.language} />;
          })}
        </View>
        <View style={{ height: 250 }} />
        <Modal />
      </ScrollView>
    );
  if (student && !student?.isStudent)
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} opacity={profileModal ? 0.5 : 1}>
          <View style={styles.imageContainer2}>
            <ImageBackground
              style={styles.taskImage}
              source={require("../assets/profileBG.png")}
            />
          </View>

          <Avatar
            title
            rounded
            title={student.firstName.charAt(0) + student.lastName.charAt(0)}
            size={130}
            activeOpacity={0.7}
            containerStyle={styles.avatar}
          />

          <View style={styles.topBox}>
            <Text style={styles.username}>
              {student.firstName + " " + student.lastName}
            </Text>
            <Text style={styles.secondaryText}>{student.jobTitle}</Text>
            <Rating imageSize={20} size={20} style={{ paddingVertical: 10 }} />
            <Text style={styles.secondaryText}>4.8 (10)</Text>
          </View>
          <Divider />
          <View style={styles.box}>
            <View style={styles.row}>
              <Text style={styles.title}>Bio</Text>
              {edit ? (
                <Icon
                  name={"create-outline"}
                  type="Ionicons"
                  style={styles.icon}
                  onPress={() => navigation.navigate("Bio")}
                />
              ) : null}
            </View>
            <Text style={styles.text}>{student.bio}</Text>
          </View>

          <View style={{ height: 250 }} />
          <Modal />
        </ScrollView>
      </View>
    );
  return null;
};
const styles = StyleSheet.create({
  row: { flexDirection: "row", justifyContent: "space-between" },
  icon: {
    color: "black",
    fontSize: 20,
    alignSelf: "center",
    marginRight: 7,
    marginBottom: 20,
  },
  language: { flexDirection: "row", alignItems: "center" },
  point: {
    backgroundColor: "#C1C5F2",
    width: 10,
    height: 10,
    borderRadius: 30,
    marginRight: 10,
  },
  drop: {
    backgroundColor: "black",
  },
  text: {
    lineHeight: 25,
    fontSize: 16,
  },
  topBox: {
    alignContent: "center",
    alignItems: "center",
    marginTop: "17%",
  },
  taskImage: {
    height: "100%",
    width: "100%",
    zIndex: -1,
  },
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  imageContainer: {
    zIndex: -1,
    width: "100%",
    height: "12%",
    backgroundColor: "#FFFDF9",
  },
  imageContainer2: {
    zIndex: -1,
    width: "100%",
    height: "20%",
    backgroundColor: "#FFFDF9",
  },
  secondaryText: {
    lineHeight: 18,
    fontSize: 14,
    color: "#797979",
    marginBottom: 5,
  },
  title: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 15,
  },
  username: {
    fontSize: 14,
    fontWeight: "500",
    marginBottom: 5,
  },
  avatar: {
    position: "absolute",
    top: "4%",
    alignSelf: "center",
    justifyContent: "center",
    backgroundColor: "#C2CFF4",
  },
  avatar2: {
    position: "absolute",
    top: "10%",
    alignSelf: "center",
    justifyContent: "center",
    backgroundColor: "#C2CFF4",
  },
  topText: {
    position: "relative",
    alignContent: "center",
    marginTop: 20,
  },
  box: {
    paddingLeft: "7%",
    paddingRight: "7%",
  },
});
export default StudentProfile;
