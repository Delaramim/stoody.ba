import React, { useState, useEffect } from "react";
import { View, StyleSheet } from "react-native";
import * as firebase from "firebase";
import {
  Content,
  Tab,
  Tabs,
  DefaultTabBar,
  Text,
  Button,
  Icon,
} from "native-base";
import TaskCard from "../components/TaskCard";

import { useTaskContext, useAuthContext } from "../context";

const MyTasks = ({ navigation }) => {
  const { student, setStudent, user } = useAuthContext();
  const [completedTasks, setCompletedTasks] = useState(null);
  const [notCompletedTasks, setNotCompletedTasks] = useState(null);
  const [person, setPerson] = useState(
    student?.isStudent ? "student" : "employer"
  );

  const getCompletedTasks = () => {
    console.log("Perspn", person);
    const id = student.id;
    const unsubscribe = firebase
      .firestore()
      .collection("Tasks")
      .where(person, "==", id)
      .where("completed", "==", true)
      .onSnapshot((snapshot) => {
        const taskList = snapshot.docs.map((doc) => ({
          id: doc.id,
          name: doc.data().name,
          description: doc.data().description,
          compensation: doc.data().compensation,
          requirements: doc.data().requirements,
          location: doc.data().location,
          employer: doc.data().employer,
          student: doc.data().student,
          assigned: doc.data().assigned,
          completed: doc.data().completed,
          // createdAt: doc.data().createdAt,
        }));
        setCompletedTasks(taskList);
      });

    return () => unsubscribe();
  };

  const getNotCompletedTasks = () => {
    const id = student.id;
    const unsubscribe = firebase
      .firestore()
      .collection("Tasks")
      .where(person, "==", id)
      .where("completed", "==", false)
      .onSnapshot((snapshot) => {
        const taskList = snapshot.docs.map((doc) => ({
          id: doc.id,
          name: doc.data().name,
          description: doc.data().description,
          compensation: doc.data().compensation,
          requirements: doc.data().requirements,
          location: doc.data().location,
          employer: doc.data().employer,
          student: doc.data().student,
          assigned: doc.data().assigned,
          completed: doc.data().completed,
          // createdAt: doc.data().createdAt,
        }));
        setNotCompletedTasks(taskList);
      });

    return () => unsubscribe();
  };

  useEffect(() => {
    getCompletedTasks();
    getNotCompletedTasks();
  }, []); // <----- empty dependency means it only runs on first mount
  //  console.log(tasks);
  const renderTabBar = (props) => {
    props.tabStyle = Object.create(props.tabStyle);
    return <DefaultTabBar {...props} />;
  };

  return (
    <View style={styles.container}>
      <Tabs renderTabBar={renderTabBar}>
        <Tab heading="New">
          <Content style={styles.container}>
            {!student?.isStudent ? (
              <Button
                style={styles.button}
                onPress={() => navigation.navigate("CreateTask")}
              >
                <Icon
                  style={styles.icon}
                  active
                  name="add-outline"
                  type="Ionicons"
                />
                <Text style={styles.buttonText}>Create a task</Text>
              </Button>
            ) : null}
            {notCompletedTasks?.map((task) => {
              return (
                <TaskCard navigation={navigation} task={task} key={task.id} />
              );
            })}
          </Content>
        </Tab>
        <Tab heading="Completed">
          <Content style={styles.container}>
            {completedTasks?.map((task) => {
              return (
                <TaskCard navigation={navigation} task={task} key={task.id} />
              );
            })}
          </Content>
        </Tab>
      </Tabs>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: "#F3F5FB" },
  icon: {
    fontSize: 20,
    color: "white",
    fontWeight: "600",
    marginLeft: 5,
    marginRight: 5,
  },
  button: {
    marginTop: 20,
    backgroundColor: "#34C787",
    borderRadius: 10,
    width: "90%",
    height: 45,
    alignSelf: "center",
    justifyContent: "center",
    fontSize: 14,
    borderWidth: 0.2,
    borderColor: "#34C787",
  },
  buttonText: { fontSize: 16, fontWeight: "600", color: "white" },
});

export default MyTasks;
