import React, { useState, useEffect } from "react";
import { Rating, Avatar } from "react-native-elements";
import { StyleSheet, ImageBackground, View, ScrollView } from "react-native";
import { Text } from "native-base";
import Divider from "../components/Divider";

import Experience from "../components/Experience";
import Skill from "../components/Requirement";

import BulletedText from "../components/BulletedText";

const PersonProfile = ({ route }) => {
  const person = route.params.person;

  if (person && person.isStudent)
    return (
      <ScrollView style={styles.container}>
        <View style={styles.imageContainer}>
          <ImageBackground
            style={styles.taskImage}
            source={require("../assets/profileBG.png")}
          />
        </View>

        <Avatar
          title
          rounded
          title={person.firstName.charAt(0) + person.lastName.charAt(0)}
          size={130}
          activeOpacity={0.7}
          containerStyle={styles.avatar}
        />

        <View style={styles.topBox}>
          <Text style={styles.username}>
            {person.firstName + " " + person.lastName}
          </Text>
          <Text style={styles.secondaryText}>{person.jobTitle}</Text>
          <Rating imageSize={20} size={20} style={{ paddingVertical: 10 }} />
          <Text style={styles.secondaryText}>4.8 (10)</Text>
        </View>
        <Divider />
        <View style={styles.box}>
          <Text style={styles.title}>Bio</Text>
          <Text style={styles.text}>{person.bio}</Text>
        </View>
        <Divider />
        <View style={styles.box}>
          <Text style={styles.title}>Experience</Text>
          {person.experiences.map((ex, index) => {
            return (
              <Experience
                key={index}
                job={ex.title}
                company={ex.company}
                from={ex.from}
                till={ex.to}
              />
            );
          })}
        </View>
        <Divider />
        <View style={styles.box}>
          <Text style={styles.title}>Education</Text>
          {person.educations.map((ed, index) => {
            return (
              <Experience
                key={index}
                job={ed.degree}
                company={ed.school}
                time={ed.study}
              />
            );
          })}
        </View>
        <Divider />
        <View style={styles.box}>
          <Text style={styles.title}>Skills</Text>
          {person.skills.map((skill, index) => {
            return <Skill key={index} text={skill.skill} />;
          })}
        </View>
        <Divider />
        <View style={styles.box}>
          <Text style={styles.title}>Languages</Text>
          {person.languages.map((lan, index) => {
            return <BulletedText key={index} languageName={lan.language} />;
          })}
        </View>
        <View style={{ height: 250 }} />
      </ScrollView>
    );
  if (person && !person.isStudent)
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container}>
          <View style={styles.imageContainer2}>
            <ImageBackground
              style={styles.taskImage}
              source={require("../assets/profileBG.png")}
            />
          </View>

          <Avatar
            title
            rounded
            title={person.firstName.charAt(0) + person.lastName.charAt(0)}
            size={130}
            activeOpacity={0.7}
            containerStyle={styles.avatar2}
          />

          <View style={styles.topBox}>
            <Text style={styles.username}>
              {person.firstName + " " + person.lastName}
            </Text>
            <Text style={styles.secondaryText}>{person.jobTitle}</Text>
            <Rating imageSize={20} size={20} style={{ paddingVertical: 10 }} />
            <Text style={styles.secondaryText}>4.8 (10)</Text>
          </View>
          <Divider />
          <View style={styles.box}>
            <Text style={styles.title}>Bio</Text>
            <Text style={styles.text}>{person.bio}</Text>
          </View>

          <View style={{ height: 250 }} />
        </ScrollView>
      </View>
    );
  return null;
};
const styles = StyleSheet.create({
  language: { flexDirection: "row", alignItems: "center" },
  point: {
    backgroundColor: "#C1C5F2",
    width: 10,
    height: 10,
    borderRadius: 30,
    marginRight: 10,
  },
  drop: {
    backgroundColor: "black",
  },
  text: {
    lineHeight: 25,
    fontSize: 16,
  },
  topBox: {
    alignContent: "center",
    alignItems: "center",
    marginTop: "13%",
  },
  taskImage: {
    height: "100%",
    width: "100%",
    zIndex: -1,
  },
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  imageContainer: {
    zIndex: -1,
    width: "100%",
    height: "12%",
    backgroundColor: "#FFFDF9",
  },
  imageContainer2: {
    zIndex: -1,
    width: "100%",
    height: "25%",
    backgroundColor: "#FFFDF9",
  },
  secondaryText: {
    lineHeight: 18,
    fontSize: 14,
    color: "#797979",
    marginBottom: 5,
  },
  title: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 15,
  },
  username: {
    fontSize: 14,
    fontWeight: "500",
    marginBottom: 5,
  },
  avatar: {
    position: "absolute",
    top: "4%",
    alignSelf: "center",
    justifyContent: "center",
    backgroundColor: "#C2CFF4",
  },
  avatar2: {
    position: "absolute",
    top: "10%",
    alignSelf: "center",
    justifyContent: "center",
    backgroundColor: "#C2CFF4",
  },
  topText: {
    position: "relative",
    alignContent: "center",
    marginTop: 20,
  },
  box: {
    paddingLeft: "7%",
    paddingRight: "7%",
  },
});
export default PersonProfile;
