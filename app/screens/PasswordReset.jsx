import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { View, StyleSheet, Image } from "react-native";
import { Text, Form, Icon } from "native-base";
import { useAuthContext, useUserContext } from "../context";
import MyButton from "../components/Button";
import FormInput from "../components/FormIconInput";

const LoginScreen = ({ navigation }) => {
  const { control, handleSubmit, errors } = useForm();
  const [skills, setSkills] = useState([]);
  const { doPasswordReset, passwordSuccess, error, setError } =
    useAuthContext();

  const onSubmit = (data) => {
    doPasswordReset({ email: data.email });
  };
  useEffect(() => {
    setError(" ");
  }, []);
  return (
    <View style={styles.container}>
      <Image style={styles.topImage} source={require("../assets/WTop.png")} />

      {!passwordSuccess ? (
        <View style={styles.box}>
          <Text style={styles.titleBig}>Reset your password</Text>
          <Text style={styles.secondary}>
            Enter your email to receive a password reset link.
          </Text>
          <Form>
            <View style={styles.formInputs}>
              <FormInput
                placeholder="email"
                name={"email"}
                control={control}
                autoCapitalize={"none"}
                iconName={"mail-outline"}
              />
            </View>
            <Text style={styles.error}>{error}</Text>
            <View style={styles.buttons}>
              <MyButton
                width={280}
                title={"Confirm"}
                backgroundColor={"#34C787"}
                onPress={handleSubmit(onSubmit)}
                color={"white"}
              />
            </View>
          </Form>
        </View>
      ) : (
        <View style={styles.box}>
          <View style={{ marginBottom: "20%", marginTop: "20%" }}>
            <Icon
              style={{ color: "#34C787", fontSize: 150, alignSelf: "center" }}
              name="checkmark-circle"
            />
            <Text style={styles.secondary}>Email successfully sent!</Text>
          </View>

          <MyButton
            width={280}
            title={"Login"}
            backgroundColor={"#34C787"}
            onPress={() => {
              navigation.navigate("Login"), setError(" ");
            }}
            color={"white"}
          />
        </View>
      )}

      <Image
        style={styles.bottomImage}
        source={require("../assets/WBottom.png")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  formInputs: { marginTop: "20%", marginBottom: "0%" },
  error: {
    fontSize: 12,
    color: "red",
    alignSelf: "center",
    marginBottom: 20,
  },
  box: {
    padding: "10%",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    marginTop: "30%",
  },
  titleBig: {
    fontSize: 30,
    fontWeight: "600",
    marginBottom: "5%",
  },

  bottomImage: {
    marginTop: "90%",
    height: 78.68,
    width: "100%",
    position: "absolute",
    bottom: 0,
  },
  topImage: {
    height: 110,
    width: "100%",
    position: "absolute",
    top: 0,
    zIndex: 1,
  },

  buttons: {
    alignSelf: "center",
    width: "100%",
    marginBottom: "35%",
    marginTop: "0%",
  },
  register: {
    alignSelf: "center",
    marginTop: 20,
    fontSize: 20,
    color: "#34C787",
    textDecorationLine: "underline",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  secondary: {
    fontSize: 18,
    textAlign: "center",
  },
});

export default LoginScreen;
