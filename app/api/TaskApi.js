import * as firebase from "firebase";

export function addTask(data) {
  if (data)
    firebase
      .firestore()
      .collection("Tasks")
      .add({
        employer: data.employer,
        name: data.name,
        compensation: data.compensation,
        description: data.description,
        requirements: data.requirements,
        location: data.location,
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
        assigned: false,
        student: null,
        completed: false,

        // requirements: requirements,
      })
      .then(() => {
        console.log("Document successfully written!");
      })
      .catch((error) => {
        console.log("Error writing document: ", error);
      });
}
export function applyTask(data) {
  if (data.id)
    firebase
      .firestore()
      .collection("Tasks")
      .doc(data.id)
      .update({
        assigned: true,
        student: data.studentID,
      })

      .then(() => {
        console.log("Document successfully written!");
      })
      .catch((error) => {
        console.log("Error writing document: ", error);
      });
}

export function completeTask(data) {
  console.log("Complete", data);
  if (data)
    firebase
      .firestore()
      .collection("Tasks")
      .doc(data)
      .update({
        completed: true,
      })

      .then(() => {
        console.log("Document successfully written!");
      })
      .catch((error) => {
        console.log("Error writing document: ", error);
      });
}
export function deleteTask(data) {
  if (data)
    firebase
      .firestore()
      .collection("Tasks")
      .doc(data.id)
      .delete()
      .then(() => {
        console.log("Document successfully deleted!");
      })
      .catch((error) => {
        console.log("Error removing document: ", error);
      });
}
export function EditTask(data) {
  if (data.id)
    firebase
      .firestore()
      .collection("Tasks")
      .doc(data.id)
      .update({
        name: data.data.taskName,
        compensation: data.data.compensation,
        description: data.data.taskDescription,
        requirements: data.requirements,
        location: data.data.location,
      })

      .then(() => {
        console.log("Document successfully written!");
      })
      .catch((error) => {
        console.log("Error writing document: ", error);
      });
}

export function Updates() {
  if (data.id) firebase.firestore().collection("Tasks");
}
