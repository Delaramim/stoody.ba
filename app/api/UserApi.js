import * as firebase from "firebase";
require("firebase/auth");

export function addStudent(data) {
  // console.log(data);
  if (data)
    firebase
      .firestore()
      .collection("Users")
      .doc(data.id)
      .set({
        id: data.id,
        lastName: data.lastName,
        firstName: data.firstName,
        bio: data.bio,
        jobTitle: data.jobTitle,
        mainServices: data.mainServices,
        skills: data.skills,
        languages: data.languages,
        experiences: data.experiences,
        educations: data.educations,
        isStudent: data.isStudent,
        token: " ",
      })
      .catch((error) => consol.log(error));
}

export function addEmployer(data) {
  if (data)
    firebase
      .firestore()
      .collection("Users")
      .doc(data.id)
      .set({
        id: data.id,
        lastName: data.lastName,
        firstName: data.firstName,
        bio: data.bio,
        jobTitle: data.jobTitle,
        isStudent: data.isStudent,
      })
      .catch((error) => consol.log(error));
}
export async function fetchUser(data, getStudent) {
  return firebase
    .firestore()
    .collection("Users")
    .doc(data)
    .get()
    .then((snapshot) => {
      if (snapshot.exists) {
        // console.log(snapshot.data());
        getStudent(snapshot.data());
      } else {
        console.log("does not exist");
        return null;
      }
    });
}

export function EditBio(data) {
  if (data.id)
    firebase
      .firestore()
      .collection("Users")
      .doc(data.id)
      .update({
        bio: data.data.bio,
        jobTitle: data.data.workMain,
        mainServices: data.data.workSecondary,
      })

      .then(() => {
        console.log("Document successfully written!");
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
      });
}

export function EditEducations(data) {
  if (data.id)
    firebase
      .firestore()
      .collection("Users")
      .doc(data.id)
      .update({
        educations: data.educations,
      })

      .then(() => {
        console.log("Document successfully written!");
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
      });
}
export function EditExperiences(data) {
  if (data.id)
    firebase
      .firestore()
      .collection("Users")
      .doc(data.id)
      .update({
        experiences: data.experiences,
      })

      .then(() => {
        console.log("Document successfully written!");
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
      });
}

export function EditSkills(data) {
  if (data.id)
    firebase
      .firestore()
      .collection("Users")
      .doc(data.id)
      .update({
        skills: data.skills,
      })

      .then(() => {
        console.log("Document successfully written!");
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
      });
}

export function EditLanguages(data) {
  if (data.id)
    firebase
      .firestore()
      .collection("Users")
      .doc(data.id)
      .update({
        languages: data.languages,
      })

      .then(() => {
        console.log("Document successfully written!");
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
      });
}

export function addToken(data) {
  if (data.id)
    firebase
      .firestore()
      .collection("Users")
      .doc(data.id)
      .update({
        token: data.expoPushToken,
      })

      .then(() => {
        console.log("Document successfully written!");
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
      });
}
// export function addStudent() {
//   firebase
//     .firestore()
//     .collection("Students")

//     .set({
//       key: "1",
//       value: "",
//     })
//     .then((ref) => {
//       console.log(ref);
//     })
//     .catch((error) => consol.log(error));
// }
