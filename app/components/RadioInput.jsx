import React from "react";
import { Image } from "react-native";
import { View, StyleSheet } from "react-native";
import { useUserContext } from "../context";

import { Text, Button, Form, Item, Input, Label, Icon } from "native-base";

const RadioInput = () => {
  const { isStudent, setIsStudent } = useUserContext();
  return (
    <View style={styles.radioContainer}>
      <Text style={styles.text}>I want to</Text>
      <View style={styles.row}>
        <Button
          style={isStudent ? styles.selected : styles.notSelected}
          onPress={() => setIsStudent(true)}
        >
          <Text style={!isStudent ? styles.textPrimary : styles.textSecondary}>
            Work
          </Text>
        </Button>
        <Button
          style={!isStudent ? styles.selected : styles.notSelected}
          onPress={() => setIsStudent(false)}
        >
          <Text style={isStudent ? styles.textPrimary : styles.textSecondary}>
            Hire
          </Text>
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  notSelected: {
    borderRadius: 0,
    borderWidth: 0.3,
    width: "50%",
    backgroundColor: "white",
    borderColor: "#47AB7F",
  },

  selected: {
    borderRadius: 0,
    borderWidth: 0.3,
    width: "50%",
    backgroundColor: "#47AB7F",
    borderColor: "#47AB7F",
  },
  text: {
    fontSize: 16,
    fontWeight: "500",
    color: "black",
    marginBottom: 10,
    alignSelf: "center",
  },
  textSecondary: {
    fontSize: 16,
    fontWeight: "500",
    color: "white",
    alignSelf: "center",
  },
  textPrimary: { fontSize: 16, fontWeight: "500", color: "#47AB7F" },
  radioContainer: {
    alignContent: "center",
    justifyContent: "center",
    marginBottom: 30,
  },
  radioRight: {
    borderRadius: 0,
  },
  row: {
    flexDirection: "row",
    flexWrap: "wrap",
    width: "60%",
    alignSelf: "center",
    borderRadius: 20,
  },
});

export default RadioInput;
