import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import { Icon } from "native-base";

const ProfileIcon = ({ navigation }) => {
  const [profile, setProfile] = useState("person-outline");
  const [opened, setOpened] = useState(false);
  const handlePress = () => {
    if (opened) {
      setProfile("person-outline"), setOpened(false);
    } else {
      setProfile("person"),
        setOpened(true),
        navigation.navigate("StudentProfile");
    }
  };
  return (
    <View style={styles.spacing}>
      <Icon
        name={profile}
        type="Ionicons"
        size={20}
        color="#B56E52"
        onPress={() => handlePress()}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  spacing: {
    marginRight: 20,
  },
});
export default ProfileIcon;
