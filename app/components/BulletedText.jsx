import React from "react";
import { Text, View, StyleSheet } from "react-native";

const BulletedText = ({ languageName }) => {
  return (
    <View style={styles.language}>
      <View style={styles.point}></View>
      <Text style={styles.text}>{languageName}</Text>
    </View>
  );
};

export default BulletedText;

const styles = StyleSheet.create({
  language: { flexDirection: "row", alignItems: "center", marginBottom: 13 },
  point: {
    backgroundColor: "#4356C9",
    width: 13,
    height: 13,
    borderRadius: 30,
    marginRight: 10,
  },
  drop: {
    backgroundColor: "black",
  },
  text: {
    lineHeight: 20,
    fontSize: 14,
  },
});
