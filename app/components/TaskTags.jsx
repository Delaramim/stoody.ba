import React from "react";
import { View, StyleSheet, Text } from "react-native";

import { Icon } from "native-base";

const TaskTags = ({ icon, title, input }) => {
  return (
    <View style={styles.alignColum}>
      <Icon style={styles.icon} name={icon} type="Ionicons" color="#333333" />
      <Text style={styles.text}>{title} </Text>
      <Text style={styles.amountText}>{input}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  alignColum: {
    flexDirection: "row",
    alignItems: "center",
  },
  spacing: { marginBottom: 10 },
  textBox: {
    marginLeft: 5,
  },
  text: {
    color: "#808080",
    lineHeight: 20,
    fontSize: 14,
    marginBottom: 10,
  },
  amountText: {
    color: "#333333",
    fontWeight: "500",
    lineHeight: 20,
    fontSize: 14,
    marginBottom: 10,
  },
  icon: {
    fontSize: 20,
    color: "black",
    fontWeight: "600",
    color: "#808080",
    marginRight: 5,
    marginBottom: 10,
  },
});

export default TaskTags;
