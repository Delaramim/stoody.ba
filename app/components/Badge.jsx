import React from "react";
import { Text, View, StyleSheet } from "react-native";

const Badge = ({ text, backgroundColor }) => {
  return (
    <View style={[styles.dueDate, { backgroundColor: backgroundColor }]}>
      <Text style={styles.text}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  dueDate: {
    fontSize: 8,
    borderRadius: 5,
    alignSelf: "flex-end",
    color: "white",
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    marginRight: 5,
  },
  text: { fontSize: 10, color: "white" },
});
export default Badge;
