import React, { useState } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";

import { useNavigation } from "@react-navigation/core";
import Button from "../components/Button";

const Footer = ({ text, onPress, backgroundColor, color }) => {
  const navigation = useNavigation();

  return (
    <View style={styles.footer}>
      <View style={styles.button}>
        <Button
          width={280}
          title={text}
          onPress={onPress}
          backgroundColor={backgroundColor}
          color={color}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  button: { height: 50, alignSelf: "center" },

  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "white",
    width: "100%",
    height: "10%",
    alignContent: "center",
    justifyContent: "center",
    paddingBottom: "13%",
    paddingTop: "10%",
  },
});

export default Footer;
