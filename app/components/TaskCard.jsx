import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import Badge from "../components/Badge";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Body,
  Text,
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";

const TaskCard = ({ navigation, task }) => {
  const { name, description, compensation, dueDate } = task;

  return (
    <TouchableOpacity onPress={() => navigation.navigate("Task", { task })}>
      <Card transparent style={styles.card}>
        <CardItem>
          <Grid>
            <Body style={{ margin: (5, 10) }}>
              <Row>
                {name.length >= 37 ? (
                  <Text style={styles.title}>{name.slice(0, 37)}...</Text>
                ) : (
                  <Text style={styles.title}>{name}</Text>
                )}
              </Row>
              <Row>
                {description.length >= 77 ? (
                  <Text>{description.slice(0, 65)}...</Text>
                ) : (
                  <Text>{description}</Text>
                )}
              </Row>
              <Row style={{ marginTop: 10 }}>
                <Badge backgroundColor={"#474CC1"} text={compensation} />
              </Row>
            </Body>
          </Grid>
        </CardItem>
      </Card>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: {
    marginTop: 15,
    alignSelf: "center",
    width: "93%",
    borderRadius: 15,
    shadowOffset: { width: 0, height: 10 },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    shadowColor: "rgba(0, 0, 0, 0.05)",
    borderLeftWidth: 10,
    borderLeftColor: "#474CC1",
    borderRightWidth: 10,
    borderRightColor: "white",
  },

  title: {
    fontWeight: "bold",
    marginBottom: 8,
  },
  date: {
    fontWeight: "400",
    fontSize: 10,
    color: "#808080",
  },
});

export default TaskCard;
