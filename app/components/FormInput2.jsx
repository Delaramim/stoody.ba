import React from "react";
import { Image } from "react-native";
import { View, StyleSheet, ScrollView } from "react-native";
import { useForm, Controller } from "react-hook-form";
import { Item, Input, Label, Icon, Text } from "native-base";

const FormInput = ({
  name,
  control,
  placeholder,
  secureTextEntry,
  text,
  autoCompleteType,
  autoCapitalize,
}) => {
  return (
    <View>
      <Controller
        control={control}
        render={({ onChange, onBlur, value }) => (
          <Item regular style={styles.inputBox}>
            <Input
              secureTextEntry={secureTextEntry}
              placeholder={placeholder}
              onChangeText={(value) => onChange(value)}
              value={value}
              autoCompleteType={autoCompleteType}
              autoCorrect={false}
              autoCapitalize={autoCapitalize}
            />
          </Item>
        )}
        name={name}
        rules={{ required: false }}
        defaultValue=""
      />
    </View>
  );
};

const styles = StyleSheet.create({
  secondaryTitle: {
    marginTop: 5,
    fontWeight: "600",
    lineHeight: 25,
    fontSize: 14,

    marginBottom: 5,
  },

  inputBox: {
    borderRadius: 5,
    paddingLeft: "5%",
    paddingRight: "5%",
    height: 43,
    marginBottom: 25,
    borderBottomWidth: 0.5,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
  },

  inputStyle: {
    borderWidth: 0,
    padding: "5%",
  },
  icon: {
    fontSize: 14,
    color: "#656565",
  },
  inputText: { fontSize: 14 },
});
export default FormInput;
