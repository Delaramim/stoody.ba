import React from "react";
import { StyleSheet, ImageBackground, View, Text } from "react-native";

const Experience = ({ job, from, till, company, time }) => {
  return (
    <View style={{ marginBottom: 10 }}>
      <Text style={styles.job}>{job}</Text>
      <Text style={styles.secondaryText}>
        {company} {time}
      </Text>
      {till ? (
        <Text style={styles.secondaryText}>
          {from} - {till}
        </Text>
      ) : null}
    </View>
  );
};
const styles = StyleSheet.create({
  job: {
    fontSize: 14,
    fontWeight: "500",
    marginBottom: 5,
  },
  secondaryText: {
    lineHeight: 20,
    fontSize: 14,
    color: "#797979",
    marginBottom: 5,
  },
});

export default Experience;
