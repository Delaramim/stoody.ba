import React from "react";
import { View } from "react-native";

const Steps = ({ percentage }) => {
  return (
    <View
      style={{
        backgroundColor: "#D6D8F0",
        width: "100%",
        height: "1.1%",
        position: "absolute",
        top: 0,
        right: 0,
        left: 0,
        zIndex: 2,
      }}
    >
      <View
        style={{
          backgroundColor: "#5B6ADE",
          width: `${percentage}`,
          height: "100%",
        }}
      ></View>
    </View>
  );
};

export default Steps;
