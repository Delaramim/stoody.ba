import React from "react";
import { Image } from "react-native";
import { View, StyleSheet, ScrollView } from "react-native";
import { useForm, Controller } from "react-hook-form";
import { Item, Input, Label, Icon, Text } from "native-base";

const FormInput = ({
  name,
  control,
  placeholder,
  secureTextEntry,
  text,
  autoCompleteType,
  autoCapitalize,
  required,
  defaultValue,
}) => {
  return (
    <View>
      <Text style={styles.secondaryTitle}>{text}</Text>
      <Controller
        control={control}
        render={({ onChange, onBlur, value }) => (
          <Item regular style={styles.inputBox}>
            <Input
              required={required}
              secureTextEntry={secureTextEntry}
              placeholder={placeholder}
              onChangeText={(value) => onChange(value)}
              value={value}
              autoCompleteType={autoCompleteType}
              autoCorrect={false}
              autoCapitalize={autoCapitalize}
            />
          </Item>
        )}
        name={name}
        rules={{ required: false }}
        defaultValue={defaultValue ? defaultValue : ""}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  secondaryTitle: {
    marginTop: 5,
    fontWeight: "600",
    lineHeight: 25,
    fontSize: 16,

    marginBottom: 5,
  },

  inputBox: {
    borderRadius: 5,
    paddingLeft: "5%",
    paddingRight: "5%",
    height: 43,
    marginBottom: 25,
  },

  inputStyle: {
    borderWidth: 0,
    padding: "5%",
  },
  icon: {
    fontSize: 14,
    color: "#656565",
  },
  inputText: { fontSize: 14 },
});
export default FormInput;
