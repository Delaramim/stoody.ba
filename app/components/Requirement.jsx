import React from "react";
import { View, StyleSheet, Text } from "react-native";

import { Icon } from "native-base";

const Requirement = ({ text }) => {
  return (
    <View style={styles.alignColum}>
      <View style={{ marginRight: 5 }}>
        <View style={styles.iconBox}>
          <Icon
            reverse
            name="checkmark-outline"
            type="Ionicons"
            size={6}
            color="#77BF1A"
            containerStyle={styles.container}
            style={styles.icon}
          />
        </View>
      </View>

      <Text style={styles.text}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  iconBox: {
    backgroundColor: "#77BF1A",
    width: 20,
    height: 20,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
  },
  icon: {
    fontSize: 15,
    color: "white",
  },
  text: {
    lineHeight: 20,
    fontSize: 14,
  },
  alignColum: {
    flexDirection: "row",
    marginTop: 20,
    alignItems: "center",
  },
  container: {
    color: "pink",
    width: 20,
    alignItems: "center",
    height: 20,
    justifyContent: "center",
  },
});

export default Requirement;
