import React, { useState } from "react";
import { Icon } from "native-base";
import { View, StyleSheet, Text, ScrollView, Modal } from "react-native";
import { useUserContext, useAuthContext } from "../context";
import IconAndText from "../components/IconAndText";
import { useNavigation } from "@react-navigation/native";

const ProfileModal = () => {
  const navigation = useNavigation();

  const { doSignOut } = useAuthContext();

  const {
    setSkills,
    setEducations,
    setLanguages,
    setExperiences,
    profileModal,
    setProfileModal,
    setEdit,
    edit,
  } = useUserContext();

  const clearData = () => {
    setEducations([]);
    setLanguages([]);
    setExperiences([]);
    setSkills([]);
    setProfileModal(false);
  };

  const handleSignOut = () => {
    doSignOut();
    setProfileModal(!profileModal);
    clearData();
  };

  const handleEdit = () => {
    setEdit(!edit);
    setProfileModal(!profileModal);
  };
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={profileModal}
      onRequestClose={() => {
        setProfileModal(!profileModal);
      }}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <View style={styles.modalHeader}>
            <Icon
              onPress={() => setProfileModal(!profileModal)}
              style={styles.closeIcon}
              active
              name="close-outline"
              type="Ionicons"
            />
          </View>
          <View style={styles.box}>
            <IconAndText
              text={"Log out"}
              onPress={() => {
                handleSignOut();
              }}
              icon={"log-out-outline"}
            ></IconAndText>
            <IconAndText
              text={"Edit Profile"}
              onPress={() => {
                handleEdit();
              }}
              icon={"create-outline"}
            ></IconAndText>
            <IconAndText text={"Wallet"} icon={"wallet-outline"}></IconAndText>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  box: {
    padding: "5%",
  },

  modalHeader: {
    padding: 20,
    width: "100%",
    height: 65,
    borderTopEndRadius: 35,
    borderTopStartRadius: 35,
  },
  centeredView: {
    flex: 1,
    marginTop: 22,
  },
  modalView: {
    marginTop: "90%",
    marginBottom: 0,
    backgroundColor: "white",
    borderRadius: 35,
    shadowColor: "#000",
    width: "100%",
    height: "100%",
    bottom: 0,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },

  icon: {
    fontSize: 20,
    color: "#34C787",
    fontWeight: "600",
    marginLeft: 5,
    marginRight: 5,
  },

  modalTitle: {
    fontWeight: "600",
    lineHeight: 30,
    fontSize: 18,
    color: "#FFFFFF",
  },
});

export default ProfileModal;
