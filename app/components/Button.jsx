import React, { useState } from "react";
import { ImageBackground, View, StyleSheet, Image } from "react-native";
import { Button, Text } from "native-base";

const MyButton = ({ title, onPress, backgroundColor, color, width }) => {
  return (
    <Button
      large
      style={[
        styles.button,
        { backgroundColor: backgroundColor, width: width },
      ]}
      onPress={onPress}
    >
      <Text style={[styles.buttonText, { color: color }]}>{title}</Text>
    </Button>
  );
};

const styles = StyleSheet.create({
  button: {
    borderRadius: 15,
    alignContent: "center",
    justifyContent: "center",
    height: 50,
    alignSelf: "center",
  },
  buttonText: {
    lineHeight: 20,
    fontSize: 20,
    fontWeight: "500",
    alignSelf: "center",
    justifyContent: "center",
  },
});

export default MyButton;
