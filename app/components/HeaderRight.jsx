import React, { useState } from "react";
import { StyleSheet } from "react-native";
import { Icon } from "native-base";
import { getFocusedRouteNameFromRoute } from "@react-navigation/native";
import { useUserContext } from "../context";

const HeaderRight = ({ route }) => {
  const routeName = getFocusedRouteNameFromRoute(route) ?? "Home";

  const { profileModal, setProfileModal } = useUserContext();

  const handleOnPress = () => {
    setProfileModal(!profileModal);
  };

  if (routeName == "StudentProfile")
    return (
      <Icon
        name="menu-outline"
        type="Ionicons"
        onPress={handleOnPress}
        style={profileModal ? styles.pressed : styles.notPressed}
      />
    );
  else return null;
};

const styles = StyleSheet.create({
  pressed: {
    marginRight: 20,
    color: "#2E7DF6",
  },
  notPressed: {
    marginRight: 20,
    color: "#CCCFD3",
  },
});
export default HeaderRight;
