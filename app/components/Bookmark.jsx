import React, { useState } from "react";

import { Icon } from "native-base";

const Bookmark = () => {
  const [bookmark, setBookmark] = useState("bookmark-outline");
  const [bookmarked, setBookmarked] = useState(false);
  const handleSetBookmark = () => {
    if (bookmarked) {
      setBookmark("bookmark-outline"), setBookmarked(false);
    } else {
      setBookmark("bookmark"), setBookmarked(true);
    }
  };
  return (
    <Icon
      name={bookmark}
      type="Ionicons"
      size={20}
      color="#B56E52"
      onPress={() => handleSetBookmark()}
    />
  );
};

export default Bookmark;
