import React from "react";
import { StyleSheet, ImageBackground, View, ScrollView } from "react-native";
import { Rating, Avatar, Divider } from "react-native-elements";

const Divider1 = () => {
  return (
    <Divider
      style={{
        backgroundColor: "#D9D9D9",
        width: "95%",
        alignSelf: "center",
        marginTop: 20,
        marginBottom: 20,
      }}
    />
  );
};

export default Divider1;
