import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Icon } from "native-base";
import { deleteTask } from "../api/TaskApi";

const TaskEditDelete = ({ onPress, task, navigation }) => {
  const id = task.id;

  return (
    <View style={styles.row}>
      <TouchableOpacity
        style={styles.row}
        onPress={() => navigation.navigate("TaskEdit", { task })}
      >
        <Icon name={"create-outline"} type="Ionicons" style={styles.icon} />
        <Text style={styles.secondaryText}>Edit</Text>
      </TouchableOpacity>
      <View className="view" style={styles.verticalLine}></View>
      <TouchableOpacity
        onPress={() => {
          deleteTask({ id }), navigation.navigate("Home");
        }}
        style={styles.row}
      >
        <Icon name={"trash-outline"} type="Ionicons" style={styles.icon} />
        <Text style={styles.secondaryText}>Delete</Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  verticalLine: {
    height: "100%",
    width: 1,
    backgroundColor: "#909090",
    marginLeft: 50,
    marginRight: 50,
  },
  row: { flexDirection: "row", alignItems: "center", alignSelf: "center" },
  icon: {
    color: "black",
    fontSize: 20,
    alignSelf: "center",
    marginRight: 7,
  },

  text: {
    lineHeight: 20,
    fontSize: 14,
  },
});

export default TaskEditDelete;
