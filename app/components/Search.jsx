import React, { useState } from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";
import { useTaskContext } from "../context";
import { Item, Input, Label, Icon } from "native-base";

import firebase from "firebase";
require("firebase/firestore");

export default function Search() {
  const { setIsSearching, setSearchedTasks } = useTaskContext();

  const fetchTasks = (search) => {
    // console.log("Search", search.length);
    if (search.length !== 0) {
      setIsSearching(true);
      firebase
        .firestore()
        .collection("Tasks")
        .where("requirements", "array-contains", search)
        .get()
        .then((snapshot) => {
          let tasks = snapshot.docs.map((doc) => {
            const data = doc.data();
            const id = doc.id;
            return { id, ...data };
          });

          setSearchedTasks(tasks);
        });
    } else setIsSearching(false);
  };
  return (
    <View>
      <Item regular style={styles.inputBox}>
        <Icon style={styles.icon} active name={"search-outline"} />
        <Input
          style={styles.inputText}
          placeholder={"Search tasks"}
          onChangeText={(search) => fetchTasks(search)}
        />
      </Item>
    </View>
  );
}

const styles = StyleSheet.create({
  inputBox: {
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0,
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 25,
    width: "92%",
    alignSelf: "center",
    marginTop: 20,
    backgroundColor: "white",
    shadowOffset: { width: 0, height: 10 },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    shadowColor: "rgba(193, 197, 242, 0.3)",
    borderColor: "white",
  },

  inputStyle: {
    borderWidth: 0,
    padding: 35,
  },
  icon: {
    fontSize: 18,
    color: "#656565",
  },
  inputText: { fontSize: 18 },
});
