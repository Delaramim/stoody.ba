import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { Icon } from "native-base";
const IconAndText = ({ text, icon, onPress }) => {
  return (
    <TouchableOpacity style={styles.box} onPress={onPress}>
      <Icon name={icon} type="Ionicons" style={styles.icon} />
      <Text style={styles.secondaryText}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  box: {
    borderBottomColor: "#D6D8F0",
    borderBottomWidth: 0.5,
    marginBottom: 10,
    flexDirection: "row",
    paddingTop: 20,
    paddingBottom: 20,
    alignItems: "center",
  },
  secondaryText: {
    color: "black",
    fontSize: 16,
    fontWeight: "500",
    marginLeft: 20,
  },
  icon: {
    color: "black",
    fontSize: 25,
    alignSelf: "center",
  },
});

export default IconAndText;
