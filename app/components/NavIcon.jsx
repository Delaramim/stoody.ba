import React from "react";
import { Icon } from "native-base";
import { View, StyleSheet } from "react-native";

const MyIcon = () => {
  return <Icon style={styles.icon} active name="add-outline" type="Ionicons" />;
};

const styles = StyleSheet.create({
  icon: { fontSize: 20, color: "pink" },
});

export default MyIcon;
