import Autocomplete from "react-native-autocomplete-input";

import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
} from "react-native";

function comp(a, b) {
  return a.toLowerCase().trim() === b.toLowerCase().trim();
}

function findMyData(query, data) {
  console.log("Data", data);
  if (query === "") {
    return [];
  }

  const regex = new RegExp(`${query.trim()}`, "i");
  return data.filter((item) => item.name.search(regex) >= 5);
}

const StarWarsMoveFinder = ({ data }) => {
  //   console.log("Data", data);
  const [query, setQuery] = useState("");
  const myData = findMyData(query, data);

  return (
    <Autocomplete
      containerStyle={styles.containerStyle}
      inputContainerStyle={styles.inputBox}
      listContainerStyle={{
        height: "100%",
        backgroundColor: "black",
        zIndex: -1,
      }}
      listStyle={{
        maxHeight: 250,
        margin: 0,
        padding: 5,
        zIndex: 3,
        borderTopWidth: 0,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        backgroundColor: "black",
      }}
      autoCapitalize="none"
      autoCorrect={false}
      data={myData.length === 1 && comp(query, myData[0].name) ? [] : myData}
      value={query}
      onChangeText={setQuery}
      placeholder="Enter Star Wars film title"
      flatListProps={{
        keyExtractor: (item) => item.id,
        renderItem: ({ item, i }) => (
          <TouchableOpacity onPress={() => setQuery(item.name)}>
            <Text style={styles.itemText}>{item.name}</Text>
          </TouchableOpacity>
        ),
      }}
    />
  );
};

const styles = StyleSheet.create({
  containerStyle: { width: 280, height: 50, zIndex: 1, flex: 1, height: "20%" },
  inputBox: {
    borderRadius: 5,
    paddingLeft: "5%",
    paddingRight: "5%",
    height: "100%",
    width: "100%",
    borderColor: "grey",
    justifyContent: "center",
    borderWidth: 0.2,
  },
  container: {
    position: "relative",
    flex: 1,
    zIndex: 1,
    width: 300,
  },
  itemText: {
    fontSize: 15,
    margin: 10,
  },

  infoText: {
    textAlign: "center",
  },
  titleText: {
    fontSize: 18,
    fontWeight: "500",
    marginBottom: 10,
    marginTop: 10,
    textAlign: "center",
  },
});

export default StarWarsMoveFinder;
