import React, { Component, useState } from "react";
import { Picker, Icon } from "native-base";
import { StyleSheet, FlatList } from "react-native";
import { Controller } from "react-hook-form";
const MyPicker = ({ title, name, control, data }) => {
  return (
    <Controller
      control={control}
      render={({ onChange, value }) => (
        <Picker
          mode="dropdown"
          iosIcon={
            <Icon
              style={styles.icon}
              active
              name="caret-down-outline"
              type="Ionicons"
            />
          }
          placeholder={title}
          style={styles.pickerBox}
          selectedValue={value}
          onValueChange={(value, itemIndex) => onChange(value)}
        >
          {data.map((i) => {
            return <Picker.Item label={i.name} value={i.name} key={i.id} />;
          })}
        </Picker>
      )}
      name={name}
      rules={{ required: false }}
      defaultValue=""
    />
  );
};

// <Picker
//   mode="dropdown"
//   iosIcon={
//     <Icon
//       style={styles.icon}
//       active
//       name="caret-down-outline"
//       type="Ionicons"
//     />
//   }
//   placeholder={title}
//   style={styles.pickerBox}
//   selectedValue={selected}
//   // onValueChange={(itemValue, itemIndex) => setSelected(itemValue)}
//   onValueChange={(value, itemIndex) => onChange(value)}
// >
//   <Picker.Item label="Wallet" value="key0" />
//   <Picker.Item label="ATM Card" value="key1" />
//   <Picker.Item label="Debit Card" value="key2" />
//   <Picker.Item label="Credit Card" value="key3" />
//   <Picker.Item label="Net Banking" value="key4" />
// </Picker>

const styles = StyleSheet.create({
  icon: {
    fontSize: 14,
    color: "#656565",
  },
  pickerBox: {
    width: "100%",
    borderWidth: 0.3,
    borderColor: "#656565",
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 5,
    marginBottom: 10,
    alignSelf: "center",
  },
});

export default MyPicker;
