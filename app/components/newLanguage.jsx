import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Steps from "./Steps";
import Picker from "./Picker";
import { useForm, Controller } from "react-hook-form";
import { Button, Icon } from "native-base";
import FormInput from "./FormInput";
import { useUserContext } from "../context";

const NewLanguage = ({ control, handleSubmit, error, name, title, data }) => {
  const { languages, setLanguages } = useUserContext();
  return (
    <View style={styles.newLanguage}>
      <Text style={styles.secondary}>Language</Text>
      <View style={styles.row}>
        <View style={{ width: "100%" }}>
          <FormInput
            placeholder="Language"
            name={"language"}
            control={control}
          />
        </View>
        {/* <View style={styles.trashBox}>
          <Icon
            style={styles.trashIcon}
            active
            name="trash-outline"
            type="Ionicons"
          />
        </View> */}
      </View>

      <Text style={styles.secondary}>Proficiency</Text>
      <Picker title={title} name={name} control={control} data={data}></Picker>
    </View>
  );
};
const styles = StyleSheet.create({
  trashBox: {
    borderWidth: 0.3,
    width: 40,
    height: 40,
    borderRadius: 50,
    alignContent: "center",
    justifyContent: "center",
    alignSelf: "center",
    borderColor: "#656565",
    marginLeft: "5%",
  },
  trashIcon: {
    fontSize: 20,
    alignSelf: "center",
    color: "#656565",
  },
  row: { flexDirection: "row" },
  newLanguage: {
    // borderTopWidth: 0.3,
    paddingBottom: 10,
    borderColor: "#656565",
  },
  icon: {
    fontSize: 20,
    color: "#B56E52",
    fontWeight: "600",
    marginLeft: 5,
    marginRight: 5,
  },
  box: {
    padding: "5%",
    alignContent: "center",
  },
  container: {
    backgroundColor: "white",
    flex: 1,
  },
  title: {
    fontWeight: "600",
    lineHeight: 30,
    fontSize: 16,
  },
  secondary: {
    marginTop: 5,
    fontWeight: "600",
    lineHeight: 30,
    fontSize: 14,
    marginBottom: 5,
  },
  button: {
    marginTop: 10,
    backgroundColor: "white",
    borderRadius: 10,
    width: "100%",
    height: 45,
    alignSelf: "center",
    justifyContent: "center",
    fontSize: 14,
    borderWidth: 0.2,
    borderColor: "#B56E52",
  },
  buttonText: { fontSize: 16, fontWeight: "600", color: "#B56E52" },
});
export default NewLanguage;
