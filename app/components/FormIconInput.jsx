import React from "react";
import { Image } from "react-native";
import { View, StyleSheet, ScrollView } from "react-native";
import { useForm, Controller } from "react-hook-form";
import { Item, Input, Label, Icon } from "native-base";

const FormInput = ({
  name,
  iconName,
  control,
  placeholder,
  secureTextEntry,
  keyboardType,
  autoCompleteType,
  autoCapitalize,
  required,
  clearButtonMode,
}) => {
  return (
    <Controller
      control={control}
      render={({ onChange, onBlur, value, onDelete }) => (
        <Item regular style={styles.inputBox}>
          <Icon style={styles.icon} active name={iconName} />
          <Input
            required={required}
            secureTextEntry={secureTextEntry}
            style={styles.inputText}
            placeholder={placeholder}
            onChangeText={(value) => onChange(value)}
            value={value}
            keyboardType={keyboardType}
            autoCorrect={false}
            autoCapitalize={autoCapitalize}
            autoCompleteType={autoCompleteType}
            clearButtonMode={clearButtonMode}
          />
        </Item>
      )}
      name={name}
      rules={{ required: false }}
      defaultValue=""
    />
  );
};

const styles = StyleSheet.create({
  inputBox: {
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0.3,
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 25,
    width: "100%",
  },

  inputStyle: {
    borderWidth: 0,
    padding: 30,
  },
  icon: {
    fontSize: 14,
    color: "#656565",
  },
  inputText: { fontSize: 16 },
});
export default FormInput;
