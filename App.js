// @refresh state
import React, { useState, useEffect } from "react";
import { ContextProvider } from "./app/context/index";
import Router from "./app/router";
import * as firebase from "firebase";
require("firebase/auth");

// import { AuthProvider } from "./providers/AuthProvider";

// Your web app's Firebase configuration
// var firebaseConfig = {
//   apiKey: "AIzaSyAyHkK-1l6XL5VhcGOOJJIQ8oPA1A7Iph4",
//   authDomain: "stoody-fbd99.firebaseapp.com",
//   projectId: "stoody-fbd99",
//   storageBucket: "stoody-fbd99.appspot.com",
//   messagingSenderId: "452166360245",
//   appId: "1:452166360245:web:d0986901294d86e53972fc",
// };
import { LogBox } from "react-native";

const firebaseConfig = {
  apiKey: "AIzaSyBHrgw2GmDb9a6lOOmfVRvXAntV5wplKrM",
  authDomain: "second-stoody.firebaseapp.com",
  databaseURL: "https://second-stoody-default-rtdb.firebaseio.com",
  projectId: "second-stoody",
  storageBucket: "second-stoody.appspot.com",
  messagingSenderId: "1002649181425",
  appId: "1:1002649181425:web:2d6d3d58f96c51742e5d50",
  measurementId: "G-P0886FMZWX",
};
// Initialize Firebase

if (firebase.apps.length == 0) {
  firebase.initializeApp(firebaseConfig);
}
export default function App() {
  LogBox.ignoreLogs(["Warning: ..."]); // Ignore log notification by message
  LogBox.ignoreAllLogs(); //Ignore all log notifications

  return (
    <ContextProvider>
      <Router />
    </ContextProvider>
  );
}
